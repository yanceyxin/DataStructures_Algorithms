# 算法
1. **定义**：算法是解决特定问题的明确步骤集合。算法的效率通常用时间复杂度和空间复杂度来衡量。
# 排序算法
1. **定义**：排序算法是计算机科学中用于对元素序列进行排序的一系列算法。排序算法在各种应用中都非常常见，从简单的数据处理到复杂的数据库和搜索引擎优化。
2. **分类**：冒泡排序（Bubble Sort）、选择排序（Selection Sort）、插入排序（Insertion Sort）、归并排序（Merge Sort）、快速排序（Quick Sort）、堆排序（Heap Sort）、希尔排序（Shell Sort）、计数排序（Counting Sort）、桶排序（Bucket Sort）、基数排序（Radix Sort）等等。
## 冒泡排序 Bubble Sort
1. **定义**：冒泡排序（Bubble Sort）是一种简单的排序算法，它通过重复遍历要排序的数列，比较每对相邻元素，并在顺序错误的情况下交换它们。这个过程会重复进行，直到没有再需要交换的元素为止，这意味着数列已经排序完成。冒泡排序的名字来源于较小的元素会逐渐“冒泡”到数列的顶端（开始位置）。
2. **步骤**：
   - 比较相邻的元素：从数列的开始位置比较每对相邻元素，如果第一个比第二个大，则交换它们的位置。
   - 遍历数列：继续比较下一对相邻元素，直到到达数列的末尾。
   - 重复遍历：重复上述步骤，但每次遍历都从上一次遍历停止的位置开始，因为最后的元素已经是最大的，不需要再比较。
   - 优化：在每次遍历中，如果一次完整的遍历都没有发生交换，说明数列已经排序完成，可以提前结束排序。
3. **复杂度**：
   - 时间复杂度：冒泡排序的平均和最坏情况时间复杂度都是 \(O(n^2)\)，其中 \(n\) 是数列的长度。这是因为算法需要进行多次遍历和比较。
   - 空间复杂度：冒泡排序的空间复杂度是 \(O(1)\)，因为它只需要一个额外的空间来存储交换的元素。
4. 示例
```cpp
#include <iostream>
#include <vector>
#include <algorithm> // 用于std::swap

// 冒泡排序函数
void bubbleSort(std::vector<int>& arr) {
    int n = arr.size();
    bool swapped;
    for (int i = 0; i < n - 1; i++) {
        swapped = false;
        for (int j = 0; j < n - i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                // 使用std::swap交换两个元素
                std::swap(arr[j], arr[j + 1]);
                swapped = true;
            }
        }
        // 如果在这一轮排序中没有发生交换，说明数组已经有序，可以提前结束
        if (!swapped) {
            break;
        }
    }
}

// 打印数组的函数
void printArray(const std::vector<int>& arr) {
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;
}

// 主函数
int main() {
    std::vector<int> arr = {64, 34, 25, 12, 22, 11, 90};

    std::cout << "Original array:\n";
    printArray(arr);

    bubbleSort(arr);

    std::cout << "Sorted array:\n";

    printArray(arr);

    return 0;
}
```
## 快速排序 Quick Sort
1. **定义**：由英国计算机科学家托尼·霍尔（Tony Hoare）在1960年提出。它的基本思想是分治法（Divide and Conquer），通过一个称为“基准”（pivot）的元素将数组分成两个子数组，使得左边子数组的所有元素都比基准小，右边子数组的所有元素都比基准大，然后递归地对这两个子数组进行快速排序。
2. **基本步骤**：
   - 选择基准：从数组中选择一个元素作为基准（pivot）。选择基准的方法可以有多种，例如选择第一个元素、最后一个元素、中间元素，或者随机选择一个元素。
   - 分区操作（Partitioning）：重新排列数组，使得所有比基准小的元素都在基准的左边，所有比基准大的元素都在基准的右边。这个过程结束后，基准元素就处于其最终位置。
   - 递归排序：递归地对基准左边和右边的子数组进行快速排序。
   - 返回：当子数组的大小减少到1或0时，递归结束，整个数组变为有序。
3. **复杂度**：平均时间复杂度为 O(nlogn)，但在最坏情况下（例如，数组已经是有序的，或者所有元素都相等）时间复杂度会退化到 O(n^2)。
4. **示例**：
```cpp
#include <iostream>
#include <vector>

// 交换两个元素
void swap(int* a, int* b) {
    int t = *a;
    *a = *b;
    *b = t;
}

// 选择基准并进行分区
int partition(std::vector<int>& arr, int low, int high) {
    // 选择最后一个元素作为基准
    int pivot = arr[high];
    int i = (low - 1);

    for (int j = low; j <= high - 1; j++) {
        // 如果当前元素小于或等于基准
        if (arr[j] <= pivot) {
            i++;    // 增加小于基准元素的索引
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    
    return (i + 1);
}

// 快速排序函数
void quickSort(std::vector<int>& arr, int low, int high) {
    if (low < high) {
        // pi 是分区后的基准索引，可以直接选择 （low+high）/2 位置作为分区索引
        int pi = partition(arr, low, high);

        // 分别对基准左边和右边的子数组进行快速排序
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

// 打印数组
void printArray(const std::vector<int>& arr) {
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;
}

// 主函数
int main() {
    std::vector<int> arr = {10, 7, 8, 9, 1, 5};
    int n = arr.size();

    std::cout << "Original array: ";
    printArray(arr);

    quickSort(arr, 0, n - 1);

    std::cout << "Sorted array: ";
    printArray(arr);

    return 0;
}
```
## 选择排序 Selection Sort
1. **定义**：是一种简单直观的排序算法。它的工作原理是：首先在未排序的元素中找到最小（或最大）的元素，存放到排序序列的起始位置，然后，再从剩余未排序元素中继续寻找最小（或最大）元素，然后放到已排序序列的末尾。以此类推，直到所有元素均排序完毕。
2. 基本步骤：
   - 从待排序的数据元素中选出最小（或最大）的一个元素，存放在序列的起始位置。
   - 从剩余未排序元素中寻找最小（或最大）元素，然后放到已排序序列的末尾。
   - 重复第二步，直到所有元素均排序完毕。
3. 示例：
```cpp
#include <iostream>
#include <vector>
#include <algorithm> // For std::swap()

void selectionSort(std::vector<int>& arr) {
    int n = arr.size();
    for (int i = 0; i < n - 1; ++i) {
        // 找到从i到数组末尾部分的最小元素的索引
        int min_idx = i;
        for (int j = i + 1; j < n; ++j) {
            if (arr[j] < arr[min_idx]) {
                min_idx = j;
            }
        }
        // 交换当前位置i和找到的最小元素的位置
        std::swap(arr[i], arr[min_idx]);
    }
}

int main() {
    std::vector<int> arr = {64, 25, 12, 22, 11};
    
    selectionSort(arr);
    
    std::cout << "Sorted array: ";
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;
    
    return 0;
}
```

## 插入排序 Insertion Sort
1. **定义**：是一种简单直观的排序算法，它的工作原理是通过构建有序序列，对于未排序数据，在已排序序列中从后向前扫描，找到相应位置并插入。插入排序在实现上，通常采用in-place排序（即只需用到O(1)的额外空间的排序），因而在从后向前扫描过程中，需要反复把已排序元素逐步向后挪位，为新元素提供插入空间。
2. **基本步骤**：
   - 从第一个元素开始，该元素可以认为已经被排序。
   - 取出下一个元素，在已经排序的元素序列中从后向前扫描。
   - 如果该元素（已排序）小于新元素，将该元素移到下一位置。
   - 重复步骤3，直到找到已排序元素小于或者等于新元素的位置。
   - 将新元素插入到该位置后。
   - 重复步骤2~5。
3. **复杂度**：时间复杂度是O(n^2)，在小规模数据或基本有序的数据中，插入排序可以表现得非常好，因为它的最好情况和平均情况时间复杂度分别为O(n)和O(n^2)。此外，插入排序是稳定的排序算法，这意味着相等的元素之间的相对顺序不会改变。
4. **示例**：
```CPP
#include <iostream>
#include <vector>

void insertionSort(std::vector<int>& arr) {
    int n = arr.size();
    for (int i = 1; i < n; ++i) {
        int key = arr[i];
        int j = i - 1;

        // 将比key大的元素向后移动一位
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        // 将key插入到正确的位置
        arr[j + 1] = key;
    }
}
/* 推算
n = 5
第一次循环
for(i=1; i<5; i++>)
{
    key=arr[i]=arr[1]=5
    j=i-1 = 1-1 = 0
    while(0>=0 && 9>5){
        arr[j+1]=arr[1]=a[0]=9
        j = j-1 = -1
    }
    arr[j+1] = arr[0] = key = 5
}
第二次循环
for(i=2; i<5; i++)
{
    key = arr[i] = arr[2] = 1
    j = i-1 = 2-1 =1

    //第一次while
    while(1>=0 && arr[1]>1)
    {
        arr[j+1]=arr[2] = arr[1] = 9
        j = j-1=0
    }
    //第二次while
    while(0>=0 && arr[0] > 1)
    {
        arr[1] = arr[0] = 5
        j = j-1 = -1
    }

    arr[j+1] = arr[0] = key = 1
}
...以此类推...

*/


int main() {
    std::vector<int> arr = {9, 5, 1, 4, 3};
    insertionSort(arr);

    std::cout << "Sorted array: ";
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;

    return 0;
}
```

## 归并排序 Merge Sort
1. **定义**：是一种分治算法，其基本思想是将两个已经排序的序列合并成一个排序的序列。归并排序的效率很高，对于大数据集来说，归并排序通常比插入排序和选择排序更快。归并排序是稳定的排序算法，这意味着相等的元素之间的相对顺序不会改变。
2. **基本步骤**：
   - 将当前序列分成两个等长的部分。
   - 对两个部分分别进行归并排序。
   - 将两个已排序的部分合并成一个排序的序列。
3. **复杂度**：归并排序的时间复杂度是O(n log n)，其中n是数组的长度。由于归并排序需要额外的存储空间来存储临时数组，因此它的空间复杂度是O(n)。
4. **示例**：
```CPP
#include <iostream>
#include <vector>

// 合并两个子序列的函数
void merge(std::vector<int>& arr, int left, int mid, int right) {
    int n1 = mid - left + 1; // 左子序列的长度
    int n2 = right - mid;   // 右子序列的长度

    // 创建临时数组
    std::vector<int> L(n1), R(n2);

    // 拷贝数据到临时数组 L[] 和 R[]
    for (int i = 0; i < n1; i++)
        L[i] = arr[left + i];
    for (int j = 0; j < n2; j++)
        R[j] = arr[mid + 1 + j];

    // 合并临时数组回到 arr[left..right]
    int i = 0; // 初始索引第一个子数组
    int j = 0; // 初始索引第二个子数组
    int k = left; // 初始索引合并的子数组

    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        } else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    // 拷贝 L[] 的剩余元素
    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    // 拷贝 R[] 的剩余元素
    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}

// l 是数组的左边界，r 是右边界
void mergeSort(std::vector<int>& arr, int left, int right) {
    if (left < right) {
        // 同 [left, mid] 和 [mid+1, right] 两个子序列
        int mid = left + (right - left) / 2;

        // 分别对两个子序列进行归并排序
        mergeSort(arr, left, mid);
        mergeSort(arr, mid + 1, right);

        // 合并两个子序列
        merge(arr, left, mid, right);
    }
}

int main() {
    std::vector<int> arr = {12, 11, 13, 5, 6};
    int arr_size = arr.size();

    mergeSort(arr, 0, arr_size - 1);

    std::cout << "Sorted array: ";
    for (int i = 0; i < arr_size; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;

    return 0;
}
```


## 堆排序 Heap Sort
1. **定义**：是一种基于比较的排序算法，使用二叉堆数据结构。堆排序分为两个阶段：建立堆（Build Max Heap）和排序（Heapsort）。堆是一个满足以下性质的完全二叉树：对于最大的堆（大顶堆），父节点的键值总是大于或等于其子节点的键值；对于最小的堆（小顶堆），父节点的键值总是小于或等于其子节点的键值。
2. **基本步骤**：
   - 建立最大堆：将无序序列构建成一个最大堆，即每个父节点的值都大于或等于其子节点的值。
   - 排序：交换堆顶元素（当前最大值）和数组末尾元素，然后对堆进行调整（堆化），使其满足最大堆的性质。重复这个过程，直到数组完全有序。
3. **复杂度**：堆排序的时间复杂度是O(n log n)，其中n是数组的长度。堆排序是原地排序，不需要额外的存储空间，因此空间复杂度是O(1)。
4. **示例**：
```cpp
#include <iostream>
#include <vector>
#include <algorithm> // For std::swap()

// 调整堆，保持最大堆性质
void heapify(std::vector<int>& arr, int n, int i) {
    int largest = i;       // 初始化最大元素为根
    int left = 2 * i + 1;  // 左子节点
    int right = 2 * i + 2; // 右子节点

    // 如果左子节点比根大，则更新最大元素
    if (left < n && arr[left] > arr[largest])
        largest = left;

    // 如果右子节点比最大元素大，则更新最大元素
    if (right < n && arr[right] > arr[largest])
        largest = right;

    // 如果最大元素不是根，交换它们，并继续调整堆
    if (largest != i) {
        std::swap(arr[i], arr[largest]);
        heapify(arr, n, largest);
    }
}

// 堆排序
void heapSort(std::vector<int>& arr) {
    int n = arr.size();

    // 构建最大堆
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // 一个个从堆顶取出元素
    for (int i = n - 1; i >= 0; i--) {
        // 将当前最大元素（堆顶）与末尾元素交换
        std::swap(arr[0], arr[i]);

        // 调整堆
        heapify(arr, i, 0);
    }
}

int main() {
    std::vector<int> arr = {12, 11, 13, 5, 6};
    heapSort(arr);

    std::cout << "Sorted array: ";
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;

    return 0;
}
```

## 希尔排序 Shell Sort
1. **定义**：是插入排序的一种优化版本，也称为缩小增量排序。它的基本思想是将原始数据分成若干子序列，这些子序列中的元素之间存在一定的间隔（称为增量），对每个子序列分别进行插入排序。随着增量逐渐减小，整个序列最终变为有序序列。
2. **基本步骤**：
   - 选择一个增量序列：增量序列的选择对算法的性能有很大影响。常见的增量序列有原始长度的一半、原始长度除以某个常数等。
   - 对子序列进行插入排序：以增量为间隔将原始序列分割成多个子序列，并对每个子序列进行插入排序。
   - 减小增量：减小增量的值，重复步骤2，直到增量为1，此时对整个序列进行插入排序。
3. **复杂度**：时间复杂度在平均情况下是O(n log n)，但在最坏情况下是O(n^2)。希尔排序是不稳定的排序算法.
4. **示例**:
```cpp
#include <iostream>
#include <vector>
#include <algorithm> // For std::swap()

// 希尔排序
void shellSort(std::vector<int>& arr) {
    int n = arr.size();
    // 增量序列，可以自定义增量序列
    std::vector<int> gaps = {1, 4, 10, 23, 57, 132, 316};
    // 也可以使用动态计算增量序列，例如：gaps[i] = n/2^i

    for (int gap : gaps) {
        for (int i = gap; i < n; i++) {
            int temp = arr[i];
            int j;
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                arr[j] = arr[j - gap];
            }
            arr[j] = temp;
        }
    }
}

int main() {
    std::vector<int> arr = {12, 11, 13, 5, 6, 7, 2, 3, 4, 1};
    shellSort(arr);

    std::cout << "Sorted array: ";
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;

    return 0;
}
``` 

## 计数排序 Counting Sort
1. **定义**：是一种非比较型排序算法，适用于一定范围内的整数排序。它的基本思想是使用一个额外的数组来统计每个值的出现次数，然后根据这些计数来确定每个元素在排序后数组中的位置。
2. 基本步骤：
    - 找出待排序数组中的最大值和最小值：确定排序的范围。
    - 创建计数数组：初始化计数数组，长度为最大值和最小值之差再加1。
    - 填充计数数组：遍历待排序数组，对于每个元素，增加计数数组中对应位置的计数。
    - 调整计数数组：将计数数组中的每个值调整为累积计数，这将给出每个元素在排序后数组中的位置。
    - 构建排序数组：根据调整后的计数数组构建排序后的数组。
3. **复杂度**：计数排序的时间复杂度是O(n + k)，其中n是数组的长度，k是数值的范围。计数排序是稳定的排序算法，并且由于它不需要比较，所以在处理大量数据且数值范围不大时非常高效。然而，当数值范围非常大时，计数排序可能会消耗大量内存
4. **示例**:
```cpp
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

// 计数排序
void countingSort(std::vector<int>& arr) {
    int n = arr.size();
    int maxVal = *std::max_element(arr.begin(), arr.end());
    int minVal = *std::min_element(arr.begin(), arr.end());
    
    // 计数数组的大小
    int range = maxVal - minVal + 1;
    std::vector<int> count(range, 0);

    // 填充计数数组
    for (int num : arr) {
        count[num - minVal]++;
    }

    // 调整计数数组为累积计数
    for (int i = 1; i < count.size(); i++) {
        count[i] += count[i - 1];
    }

    // 构建排序数组
    std::vector<int> sortedArr(n);
    for (int num : arr) {
        sortedArr[count[num - minVal] - 1] = num;
        count[num - minVal]--;
    }

    // 将排序后的数组复制回原数组
    for (int i = 0; i < n; i++) {
        arr[i] = sortedArr[i];
    }
}

int main() {
    std::vector<int> arr = {4, 2, 2, 8, 3, 3, 1};
    countingSort(arr);

    std::cout << "Sorted array: ";
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;

    return 0;
}
```

## 桶排序 Bucket Sort
1. **定义**：是一种分布式排序算法，其基本思想是将数组分为多个“桶”，每个桶负责排序数组中的一部分数据。桶排序通常用于数据服从均匀分布的情况，它通过将数据分散到有限数量的桶里，每个桶内使用其他排序算法（如插入排序）进行排序，最后按顺序合并各个桶中的数据以得到有序的序列。
2. **基本步骤**：
   - 确定桶的数量：根据数据的范围和数据个数确定桶的数量。
   - 分配数据到桶：遍历待排序数组，将每个数据分配到对应的桶中。
   - 对每个桶内的数据进行排序：通常使用插入排序或其他排序算法对桶内数据进行排序。
   - 合并桶中的数据：按顺序合并各个桶中的数据，得到最终的有序序列。
3. **复杂度**：时间复杂度取决于桶的数量和使用的排序算法，平均情况下是O(n+k)，其中n是数组的长度，k是桶的数量。桶排序是稳定的排序算法，并且由于它不需要比较，所以在处理大量数据且数值范围不大时非常高效。然而，桶排序的性能依赖于数据的分布，如果数据分布不均匀，性能可能会下降.
4. **示例**:
```cpp
#include <iostream>
#include <vector>
#include <algorithm>

void bucketSort(std::vector<int>& arr) {
    if (arr.empty()) {
        return;
    }

    // 找出最大值和最小值
    int maxVal = *std::max_element(arr.begin(), arr.end());
    int minVal = *std::min_element(arr.begin(), arr.end());

    // 计算桶的数量
    int bucketCount = (maxVal - minVal) / (arr.size() + 1) + 1;

    // 创建桶
    std::vector<std::vector<int>> buckets(bucketCount);

    // 分配数据到桶
    for (int num : arr) {
        int index = (num - minVal) / (arr.size() + 1);
        buckets[index].push_back(num);
    }

    // 对每个桶内的数据进行排序
    int idx = 0; // 用于结果数组的索引
    for (auto& bucket : buckets) {
        std::sort(bucket.begin(), bucket.end()); // 对每个桶进行排序
        for (int num : bucket) {
            arr[idx++] = num;
        }
    }
}

int main() {
    std::vector<int> arr = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3};
    bucketSort(arr);

    std::cout << "Sorted array: ";
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;

    return 0;
}
```

## 基数排序 Radix Sort
1. **定义**：是一种非比较型整数排序算法，其基本思想是将整数按位数切割成不同的数字，然后按每个位数分别比较。排序过程从最低位开始，依次对每一位进行排序，直到最高位排序完成。
2. **基本步骤**：
   - 找到最大数：确定数组中的最大数，以确定最大位数。
   - 按位排序：从最低位到最高位，对每一位进行排序。
   - 使用稳定的排序算法：通常使用计数排序或桶排序来对每一位进行排序。
   - 合并结果：经过每一位的排序后，整个数组变为有序。
3. **复杂度**：时间复杂度是O(nk)，其中n是数组的长度，k是数值的最大位数。基数排序是稳定的排序算法，并且由于它不需要比较，所以在处理大量数据且数值范围不大时非常高效。基数排序适用于整数排序，对于非整数数据，需要进行适当的转换。
4. **示例**:
```cpp
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

// 计数排序辅助函数，对特定位数进行排序
void countSort(std::vector<int>& arr, int exp) {
    const int kMax = 10; // 0-9
    std::vector<int> output(arr.size());
    std::vector<int> count(kMax, 0);

    // 统计每个数字的出现次数
    for (int i = 0; i < arr.size(); ++i) {
        int index = (arr[i] / exp) % kMax;
        count[index]++;
    }

    // 计算累积出现次数
    for (int i = 1; i < kMax; ++i) {
        count[i] += count[i - 1];
    }

    // 填充输出数组
    for (int i = arr.size() - 1; i >= 0; --i) {
        int index = (arr[i] / exp) % kMax;
        output[count[index] - 1] = arr[i];
        count[index]--;
    }

    // 将输出数组复制回原数组
    for (int i = 0; i < arr.size(); ++i) {
        arr[i] = output[i];
    }
}

// 基数排序
void radixSort(std::vector<int>& arr) {
    // 找到最大数，确定最大位数
    int maxVal = *std::max_element(arr.begin(), arr.end());
    int exp = 1; // 从最低位开始

    while (maxVal / exp > 0) {
        countSort(arr, exp); // 对每一位进行排序
        exp *= 10; // 移动到下一位
    }
}

int main() {
    std::vector<int> arr = {170, 45, 75, 90, 802, 24, 2, 66};
    radixSort(arr);

    std::cout << "Sorted array: ";
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;

    return 0;
}
```
# 分治算法
1. **定义**：分治算法（Divide and Conquer）是一种解决问题的方法论，它将一个复杂的问题分解（Divide）成一系列相同或相似的子问题，递归解决这些子问题（Conquer），然后将子问题的解合并（Combine）起来解决原问题。
2. **基本步骤**：
   - 分解：将原问题分解成若干个规模较小的相同问题。
   - 解决：递归地解决这些子问题。
   - 合并：将子问题的解合并以解决原问题。
3. **典型应用**：
   - 排序算法：
     - 归并排序：将数组分成两半，分别排序后再合并。
     - 快速排序：选择一个基准值，将数组分为两部分，一部分包含所有小于基准值的元素，另一部分包含所有大于基准值的元素，然后递归地对这两部分进行快速排序。
   - 二分查找：在有序数组中查找特定元素，通过不断将搜索区间一分为二来缩小搜索范围。
   - 最大子数组和：找出一个数组中连续子数组的最大和，可以使用分治法将问题分解为多个小问题。
   - 矩阵乘法：使用分治法优化矩阵乘法的计算复杂度。
   - Strassen算法：用于快速计算两个多项式的乘积。
   - 计算整数乘法：通过分治法减少大整数乘法的计算复杂度。
4. **案例**：
```cpp
#include <iostream>
#include <vector>

// 二分法查找特定元素的函数
int binarySearch(const std::vector<int>& arr, int target) {
    int left = 0;
    int right = arr.size() - 1;

    while (left <= right) {
        int mid = left + (right - left) / 2; // 防止溢出的写法

        if (arr[mid] == target) {
            return mid; // 找到目标值，返回索引
        } else if (arr[mid] < target) {
            left = mid + 1; // 在右半部分继续查找
        } else {
            right = mid - 1; // 在左半部分继续查找
        }
    }

    return -1; // 未找到目标值，返回-1
}
/* 推演
arr = 2 3 4 10 40 
target = 10
left=0 right= 5-1 = 4

while(0<=4)
    mid= 0 + (4-0)/2 = 2
    if(arr[2]==10)
        return mid
    else if (arr[2] < 10)
        left =mid+1 =2+1 =3
    else
        right =mid-1 = 2-1 = 1

while(3 <= 4)
    mid = 3 + (4-3)/2 = 3
    if(arr[3] == 10)
        return mid //找到目标值了，直接返回数组对应的目标值索引


*/

int main() {
    std::vector<int> arr = {2, 3, 4, 10, 40}; // 有序数组
    int target = 10; // 要查找的目标值

    int result = binarySearch(arr, target);
    if (result != -1) {
        std::cout << "元素 " << target << " 的索引是: " << result << std::endl;
    } else {
        std::cout << "元素 " << target << " 不在数组中。" << std::endl;
    }

    return 0;
}
```
# 搜索算法
1. **定义**：搜索算法是计算机科学中用于在数据结构中查找特定元素或解决特定问题的一系列算法。它们可以被分为两大类：局部搜索算法和全局搜索算法。局部搜索算法，如梯度下降法，通常只能从初始点出发找到局部最优解，而全局搜索算法则旨在找到全局最优解，如模拟退火算法（SA）、遗传算法（GA）和免疫算法（IA）。
2. **搜索算法的应用场景**
    - 数据检索：在大规模数据集中快速找到目标元素，例如在有序数组中查找目标元素、在字符串中查找特定字符、在链表中查找目标节点等。
   - 问题求解：解决各种问题，如寻找最短路径、在迷宫中找到出口、解决数独问题等，这些问题通常可以转化为搜索问题，并通过合适的搜索算法进行求解。
   - 决策优化：在多个可能的选择中找到最优解，例如使用广度优先搜索算法在社交网络中查找最短距离的好友，或使用A*搜索算法规划机器人路径等。
   - 算法设计和优化：作为算法设计和优化的基础，通过研究和理解不同类型的搜索算法，程序员可以更好地设计和优化自己的算法，提高程序的效率和性能
。
# 动态规划
1. **定义**：动态规划（Dynamic Programming，简称DP）是一种算法策略，用于解决具有重叠子问题和最优子结构特性的问题。它是一种将复杂问题分解成更简单的子问题，并通过存储这些子问题的解（通常是在表格中）来避免重复计算的方法。动态规划通常用于求解最优化问题。
2. **基本步骤**：
   - 定义状态：确定dp数组的含义，dp[i]通常表示某个子问题的解。
   - 确定状态转移方程：根据问题的特点，找出状态之间的关系，即如何从已知状态推导出新状态。
   - 确定初始条件和边界条件：初始化dp数组的值，确定递推的起点。
   - 计算dp数组的值：按照一定的顺序（通常是自底向上）填充dp数组。
   - 构造最优解：根据dp数组的值，回溯构造问题的最优解。
3. **经典题型**：
   - 斐波那契数列：计算第n个斐波那契数。
   - 背包问题：包括0/1背包问题和完全背包问题。
   - 最长公共子序列（LCS）：找出两个序列的最长公共子序列。
   - 最长递增子序列（LIS）：找出一个序列中最长的递增子序列。
   - 矩阵链乘问题：计算矩阵连乘的最少标量乘法次数。
   - 硬币找零问题：用最少的硬币数量凑成特定金额。
   - 编辑距离问题：计算将一个字符串转换成另一个字符串的最少操作次数。
   - 最短路径问题：如贝尔曼-福特算法解决的单源最短路径问题。
   - 股票买卖问题：确定在哪些天买入和卖出股票可以获得最大利润。
4. **案例**：
```cpp
#include <iostream>
#include <vector>

// 动态规划计算斐波那契数列的第n项，也可以直接用递归法
int fibonacci(int n) {
    if (n <= 1) {
        return n;
    }

    // 创建一个vector存储斐波那契数列的值
    std::vector<int> dp(n + 1);
    dp[0] = 0;
    dp[1] = 1;

    // 填充vector，从第2项开始
    for (int i = 2; i <= n; ++i) {
        dp[i] = dp[i - 1] + dp[i - 2];
    }

    return dp[n];
}

int main() {
    int n;
    std::cout << "请输入斐波那契数列的项数 n: ";
    std::cin >> n;

    std::cout << "斐波那契数列的第 " << n << " 项是: " << fibonacci(n) << std::endl;

    return 0;
}
```
# 贪心算法
1. **定义**：贪心算法是一种在每一步选择中都采取在当前状态下最好或最优（即最有利）的选择，以期望导致结果是最好或最优的算法策略。贪心算法不保证会得到最优解，但在某些问题中，贪心算法的解足够接近最优解或者确实是最优解。
2. **基本步骤**：
   - 建立数学模型：定义问题的贪心选择性质。
   - 证明贪心选择性质：证明每一步的贪心选择能导致问题的最优解。
   - 实施贪心策略：编写代码实现贪心算法。
3. **经典应用**：
   - 活动选择问题：给定一系列活动，每个活动有开始时间和结束时间，目标是选择最大数量的互不重叠的活动。
   - 霍夫曼编码：用于数据压缩，通过构建霍夫曼树来为字符分配编码，使得整体编码长度最小。
   - 最小硬币找零问题：给定不同面额的硬币和总金额，求出最少需要的硬币数量。
   - 任务调度问题：在多处理器系统中，如何调度任务以最小化完成所有任务的总时间。
   - 图的最小生成树：如Kruskal算法和Prim算法，用于在加权图中找到连接所有顶点的最小权重的生成树。
   - Dijkstra算法：用于在加权图中找到单个源点到其他所有点的最短路径。
   - 背包问题：在不超过背包容量的前提下，如何选择物品以使得总价值最大。
4. **案例**：
   对于零钱的问题：假设有 25 分、10 分、5 分、1 分这 4 种硬币，现在要从中取出 41 分钱的硬币，并且要求硬币的个数最少。
   通过贪心算法，我们每次都拿出最大额度的硬币，直到此额度超过了所需的额度。详细的过程如下：
     - 对于 41 分钱，拿出 25 分的硬币，此时还差 16 分钱，25 分的硬币超过了所需的额度，自然需要往更优、更小的额度去取；
     - 对于 16 分钱，拿出 10 分的硬币，此时还差 6 分钱，10 分的硬币超过了所需的额度，自然需要往更优、更小的额度去取；
     - 对于 6 分钱，拿出 5 分的硬币，此时还差 1 分钱，5 分的硬币超过了所需的额度，自然需要往更优、更小的额度去取；
     - 对于 1 分钱，拿出 1 分的硬币，此时还差 0 分钱；
   最终，41 分钱拆分成了 1 个 25 分、1 个 10 分、1 个 5 分、1 个 1 分，总共 4 个硬币。
```cpp
#include <iostream>
#include <vector>
#include <algorithm> // 引入算法库，用于std::sort

// 函数用于计算找零的硬币数量
void makeChange(int amount, std::vector<int>& coinValues) {
    std::vector<int> count(coinValues.size(), 0); // 用于存储每种硬币的数量

    // 从最大面额的硬币开始
    for (int i = coinValues.size() - 1; i >= 0; i--) {
        count[i] = amount / coinValues[i]; // 使用尽可能多的当前硬币
        amount %= coinValues[i]; // 更新剩余金额
    }

    // 输出每种硬币的数量
    int totalCoins = 0; // 总硬币数量
    for (int i = 0; i < count.size(); i++) {
        if (count[i] > 0) {
            std::cout << "硬币 " << coinValues[i] << " 分: " << count[i] << " 个" << std::endl;
            totalCoins += count[i];
        }
    }
    std::cout << "总共需要 " << totalCoins << " 个硬币" << std::endl;
}
/* 推演：
amount=61
coinValues=1 5 10 25
count(4, 0)
for i=3; i>=0; i--
    count[3] = amount/coinValues[3] = 61/25 = 2
    amount = amount%coinValues[3] = 61%25 = 11
    i=2
for i=2; i>=0; i--
    count[2] = amount/coinValues[2] = 11/10 = 1
    amount = amount%coinValues[2] = 11%10 = 1
    i = 1
for i=1; i>=0; i--
    count[1] = amount/coinValues[1] = 1/5 = 0
    amount = amount%coinValues[1] = 1%5 = 1
    i=0
for i=0; i>=0; i--
    count[0] = amount/coinValues[0] = 1/1 = 1
    amount = amount%coinValues[0]=1%1 = 0
    i=-1
for i=-1; i>=0; i-- 已经不成立，退出循环

输出每种硬币数量、总硬币数量
count[0] = 1 币值 1 分
count[1] = 0 币值 5 分
count[2] = 1 币值 10 分
count[3] = 2 币值 25 分
totalCoins = count[0] + count[1] + count[2] + count[3] = 1+0+1+2=4 总硬币 4 枚 

*/

int main() {
    int amount = 61; // 需要找零的金额
    std::vector<int> coinValues = {1, 5, 10, 25}; // 硬币面额

    makeChange(amount, coinValues);

    return 0;
}
```
# 回溯算法
1. **定义**：回溯算法是一种通过试错来解决问题的算法，它尝试分步的去解决一个问题。在分步解决问题的过程中，当它通过尝试发现现有的分步答案不能得到有效的正确的解答的时候，它将取消上一步甚至是上几步的计算，再通过其他的可能的分步解答再次尝试寻找问题的答案。回溯算法的基本思想是：从问题的某种状态出发，不断进行尝试，当尝试到某一步发现原先选择并不优或达不到目标时，就退回一步重新选择，再继续尝试，直到找到问题的解。
2. **基本步骤**：
   - 初始化：将问题的初始状态设置为算法的初始状态。
   - 目标条件判断：如果算法的当前状态已经满足问题的解的条件，那么输出解并结束算法。
   - 选择：从算法的当前状态开始，根据算法的设计，选择下一个状态的候选解。
   - 约束条件判断：如果选择不满足问题的约束条件，那么回溯，即退回一步，放弃当前的选择。
   - 回溯：如果当前选择不能得到问题的解，回退到上一步，选择下一个候选解。
3. **典型应用**：
   - 八皇后问题：八皇后问题是一个经典的回溯法应用，目标是在8x8的棋盘上放置8个皇后，使得它们互不攻击。回溯法通过尝试在每一行放置一个皇后，并检查是否与其他皇后冲突。如果发现冲突，则回溯到上一行，调整皇后的位置，直到找到所有可能的解决方案。这种方法体现了回溯法的核心思想：尝试、回退、再尝试。
   - 子集问题：子集问题要求找出一个集合的所有子集。回溯法可以通过递归地选择或不选择当前元素，并进入下一层决策树来解决这个问题。对于允许重复的子集问题，需要在代码中进行剪枝，以避免生成重复的子集。
   - 组合问题：组合问题要求从集合中选择若干元素，使得它们的和为特定的值。回溯法可以通过递归地选择当前元素，并更新目标值，然后继续选择下一个元素来解决这个问题。同样，对于有重复元素的情况，需要进行剪枝以避免重复的组合。
   - 排列问题：排列问题要求找出一个集合的所有可能排列。回溯法可以通过递归地选择当前元素，并将其添加到排列中，然后继续排列剩余的元素来解决这个问题。对于有重复元素的排列问题，也需要进行剪枝以避免重复的排列。
   - 划分问题：划分问题要求将一个集合分成两个或多个非空子集，使得这些子集的元素和相等。回溯法可以通过递归地选择当前元素，并将其分配给一个子集，然后继续划分剩余的元素来解决这个问题。
4. 示例：
```cpp
#include <iostream>
#include <vector>
using namespace std;

void printSubset(vector<int>& subset) {
    for (int num : subset) {
        cout << num << " ";
    }
    cout << endl;
}

void findSubsets(vector<int>& nums, int index, vector<int>& subset) {
    // 打印当前子集
    printSubset(subset);

    // 遍历所有元素
    for (int i = index; i < nums.size(); i++) {
        subset.push_back(nums[i]);
        findSubsets(nums, i + 1, subset); // 选择当前元素
        subset.pop_back(); // 回溯，移除当前元素
    }
}
/*推演：
 /*
 主函数调用函数进入
 print 空 //子集

 第一次循环
 for i=0; i<2; i++
    subset = nums[0]
    f(nums, 1, subset)//第一次递归
                        print 1 //子集
                        for i=1; i< 2; i++
                            subset = nums[1]
                            f(nums, 2, subset)//第二次递归
                                             print 1 2 //子集
                                             for i =2; i< 2; i++ for循环不成立了
                            pop 2出来，此时i=2
                        二次循环 for i =2; i< 2; i++ for循环不成立了，但此时subset 里有 1
    pop 1 出来，此时 subset没数据了

第二次循环
 for i=1; i <2; i++
    subset = nums[1]
    f(nums, 2, subset) //第三次递归
                        print 2 //子集
                        for i =2; i< 2; i++ for循环不成立了
    pop 2 出来，此时i=2，subset里也没有数据了

*/
int main() {
    vector<int> nums = {1, 2};
    vector<int> subset;
    findSubsets(nums, 0, subset);
    return 0;
}
```
# 各类算法比较
1. 分治法（Divide and Conquer）：
- 定义：将一个复杂的问题分解成两个或多个相同或相似的子问题，递归地解决这些子问题，然后将这些子问题的解合并以解决原问题。
- 优点：适用于可以分解为相似子问题的问题，可以显著减少问题的复杂度。
- 缺点：递归可能导致栈溢出，且合并子问题的结果可能需要额外的时间。
- 适用场景：排序算法（如快速排序、归并排序），二分查找等。
  
2. 动态规划（Dynamic Programming, DP）：
- 定义：将问题分解成重叠的子问题，通过解决这些子问题并将结果存储起来（通常使用表格），避免重复计算，从而提高效率。
- 优点：适用于有重叠子问题和最优子结构的问题，可以高效地解决问题。
- 缺点：需要额外的空间来存储子问题的解，且对于某些问题可能难以找到状态转移方程。
- 适用场景：背包问题，最长公共子序列问题，最短路径问题等。

3. 回溯法（Backtracking）：
- 定义：一种通过试错的方法，尝试分步解决问题。在解决问题的过程中，当发现已经选择的部分解决方案不可能产生正确的完整解决方案时，就回退到上一步或者几步，然后尝试其他可能的选项。
- 优点：不需要复杂的状态转移方程，适用于解决组合问题。
- 缺点：可能会有大量的重复计算，效率较低。
- 适用场景：八皇后问题，数独，图的着色问题等。

4. 贪心法（Greedy Algorithm）：
- 定义：在每一步选择中都采取在当前状态下最好或最优（即最有利）的选择，从而希望导致结果是全局最好或最优的算法策略。
- 优点：实现简单，运行速度快。
- 缺点：贪心选择可能导致非最优解，只适用于特定问题。
- 适用场景：活动选择问题，霍夫曼编码，最小生成树（如Kruskal算法和Prim算法）等。