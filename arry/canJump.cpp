/*
leetcode 55: https://leetcode.cn/problems/jump-game/?envType=study-plan-v2&envId=top-interview-150
题目描述：

给你一个非负整数数组 nums ，你最初位于数组的 第一个下标 。数组中的每个元素代表你在该位置可以跳跃的最大长度。

判断你是否能够到达最后一个下标，如果可以，返回 true ；否则，返回 false 。

 

示例 1：

输入：nums = [2,3,1,1,4]
输出：true
解释：可以先跳 1 步，从下标 0 到达下标 1, 然后再从下标 1 跳 3 步到达最后一个下标。
示例 2：

输入：nums = [3,2,1,0,4]
输出：false
解释：无论怎样，总会到达下标为 3 的位置。但该下标的最大跳跃长度是 0 ， 所以永远不可能到达最后一个下标。
 

提示：

1 <= nums.length <= 10^4
0 <= nums[i] <= 10^5

解题思路：
1.贪心算法： 最远可以到达的位置 大于等于数组中的最后一个位置，那就说明最后一个位置可达

*/
//==================================================2
/*
leetcode 45: https://leetcode.cn/problems/jump-game-ii/description/?envType=study-plan-v2&envId=top-interview-150
题目描述:
给定一个长度为 n 的 0 索引整数数组 nums。初始位置为 nums[0]。

每个元素 nums[i] 表示从索引 i 向前跳转的最大长度。换句话说，如果你在 nums[i] 处，你可以跳转到任意 nums[i + j] 处:

0 <= j <= nums[i] 
i + j < n
返回到达 nums[n - 1] 的最小跳跃次数。生成的测试用例可以到达 nums[n - 1]。

 

示例 1:

输入: nums = [2,3,1,1,4]
输出: 2
解释: 跳到最后一个位置的最小跳跃数是 2。
     从下标为 0 跳到下标为 1 的位置，跳 1 步，然后跳 3 步到达数组的最后一个位置。
示例 2:

输入: nums = [2,3,0,1,4]
输出: 2
 

提示:

1 <= nums.length <= 10^4
0 <= nums[i] <= 1000
题目保证可以到达 nums[n-1]

解题思路：
1. 正向查找可到达的最大位置，贪心法

*/
#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    bool canJump(vector<int>& nums) {
        int n = nums.size();
        int rightmost = 0; //目前能跳到的最远距离
        for (int i = 0; i < n; ++i) {
            if (i <= rightmost) {
                rightmost = max(rightmost, i + nums[i]);//更新最远距离
                //最远距离要是大于等于数组最后一个位置，则表示可以跳跃到数组最后位置
                if (rightmost >= n - 1) {
                    return true;
                }
            }
        }
        return false;
    }
};
//============题型2
class Solution2 {
public:
    int jump(vector<int>& nums) 
    {
        int max_far = 0;// 目前能跳到的最远位置
        int step = 0;   // 跳跃次数
        int end = 0;    // 上次跳跃可达范围右边界（下次的最右起跳点）
        for (int i = 0; i < nums.size() - 1; i++)
        {
            max_far = std::max(max_far, i + nums[i]); //更新目前能达到的最远位置
            // i位置到达上次跳跃能到达的右边界了
            if (i == end)
            {
                end = max_far;  // 目前能跳到的最远位置变成了下次起跳位置的右边界
                step++;         // 进入下一次跳跃
            }
        }
        return step;
    }
};


int main() {
    vector <int>  nums = {3, 2, 1, 0, 4 };
    Solution solution;
    bool canJump = solution.canJump(nums);
    cout << "is or not jump: " << canJump << endl;

    vector <int>  nums2 = {2, 3, 1, 1, 4};
    Solution2 solution2;
    int jump = solution2.jump(nums2);
    cout << "jump: " << jump << endl;




    return 0;
}

