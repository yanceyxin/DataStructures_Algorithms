
/*
leetcode 228. 汇总区间
给定一个  无重复元素 的 有序 整数数组 nums 。

返回 恰好覆盖数组中所有数字 的 最小有序 区间范围列表 。也就是说，nums 的每个元素都恰好被某个区间范围所覆盖，
并且不存在属于某个范围但不属于 nums 的数字 x 。

列表中的每个区间范围 [a,b] 应该按如下格式输出：

"a->b" ，如果 a != b
"a" ，如果 a == b
 

示例 1：

输入：nums = [0,1,2,4,5,7]
输出：["0->2","4->5","7"]
解释：区间范围是：
[0,2] --> "0->2"
[4,5] --> "4->5"
[7,7] --> "7"
示例 2：

输入：nums = [0,2,3,4,6,8,9]
输出：["0","2->4","6","8->9"]
解释：区间范围是：
[0,0] --> "0"
[2,4] --> "2->4"
[6,6] --> "6"
[8,9] --> "8->9"
 

提示：

0 <= nums.length <= 20
-2^31 <= nums[i] <= 2^31 - 1
nums 中的所有值都 互不相同
nums 按升序排列
*/

#include <vector>
#include <string>
#include <iostream>
using namespace std;

vector<string> summaryRanges(vector<int>& nums) {
    vector<string> result; //初始化结果容器
    int n = nums.size();
    if (n == 0) return result; //数据为空，直接返回空结果

    int start = 0; // 起始位置

    // 遍历该数组
    for (int i = 1; i <= n; ++i) {

        // 检查是否到了数组末尾或非连续元素
        //nums[i] != nums[i - 1] + 1：当前元素与前一个元素不连续，进入 if 条件判断中。
        if (i == n || nums[i] != nums[i - 1] + 1) {

            if (start == i - 1) {
                // 单个元素
                //将单个元素转换成字符串形式放到容器中
                //to_string 函数是 C++ 标准库中的一个函数，用于将数值（整数或浮点数）转换为对应的
                // 字符串格式。它是 C++11 引入的一部分，定义在 <string> 头文件中。
                result.push_back(to_string(nums[start]));
            } 
            else {
                // 区间范围
                //将当前区间 [nums[start], nums[i-1]] 转化为 "start->end" 的字符串形式添加到结果。
                result.push_back(to_string(nums[start]) + "->" + to_string(nums[i - 1]));
            }

            start = i; // 更新新的起点，表示一个新的区间开始位置
        }
    }

    return result;
}

// 测试代码
int main() {
    vector<int> nums = {0, 1, 2, 4, 5, 7};
    vector<string> result = summaryRanges(nums);

    for (const string& range : result) {
        cout << range << " ";
    }
    return 0;
}

/*
推演：
输入：{0, 1, 2, 4, 5, 7}

i     nums[i]     start            判断条件           操作      result

1       1           0         连续 (1 == 0 + 1)     不进入 if    []
2       2           0         连续 (2 == 1 + 1)     不进入 if    []
3       4           0         不连续(4 != 2+1)       进入 if     ["0->2"]
4       5           3         连续(5 == 4+1)        不进入 if    ["0->2"]
5       7           3         不连续(7 != 5+1)       进入 if     ["0->2", "4->5"]
6     超出范围        5          数组最末（i==n）       5 == 6-1    ["0->2", "4->5", "7"]
*/