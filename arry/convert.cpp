/*
leetcode 6：Z 字型变换
将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。

比如输入字符串为 "PAYPALISHIRING" 行数为 3 时，排列如下：

P   A   H   N
A P L S I I G
Y   I   R
之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："PAHNAPLSIIGYIR"。

请你实现这个将字符串进行指定行数变换的函数：

string convert(string s, int numRows);
 

示例 1：

输入：s = "PAYPALISHIRING", numRows = 3
输出："PAHNAPLSIIGYIR"
示例 2：
输入：s = "PAYPALISHIRING", numRows = 4
输出："PINALSIGYAHRPI"
解释：
P     I    N
A   L S  I G
Y A   H R
P     I
示例 3：

输入：s = "A", numRows = 1
输出："A"
 

提示：

1 <= s.length <= 1000
s 由英文字母（小写和大写）、',' 和 '.' 组成
1 <= numRows <= 1000
*/



#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
    string convert(string s, int numRows) {
        //行数等于 1 或字符串的长度小于行数，则不够形成 Z 字形，直接返回当前字符串
        if (numRows == 1 || s.size() <= numRows) return s;

        vector<string> rows(min(numRows, (int)s.size()));//rows 是一个字符串向量，用来存储 Z 字形排列中每一行的字符。
        int currentRow = 0;
        bool goingDown = false;

        //遍历字符串的每个字符 c 
        for (char c : s) {
            //将字符追加到当前行 rows[currentRow]
            rows[currentRow] += c; 

            //如果当前行是第 0 行（顶部）或第 numRows - 1 行（底部），方向需要反转
            if (currentRow == 0 || currentRow == numRows - 1) 
                goingDown = !goingDown; // Change direction at top/bottom
            
            //根据方向 goingDown 更新 currentRow，向下时 currentRow += 1，向上时 currentRow -= 1
            currentRow += goingDown ? 1 : -1;
        }

        //将所有行的字符串连接成一个字符串 result
        string result;
        for (const string& row : rows) {
            result += row;
        }
        return result;
    }
};

int main() {
    // Test cases
    Solution solution;

    string s1 = "PAYPALISHIRING";
    int numRows1 = 3;
    cout << "Input: " << s1 << ", numRows = " << numRows1 << endl;
    cout << "Output: " << solution.convert(s1, numRows1) << endl;
    cout << "Expected: PAHNAPLSIIGYIR" << endl;

    string s2 = "PAYPALISHIRING";
    int numRows2 = 4;
    cout << "\nInput: " << s2 << ", numRows = " << numRows2 << endl;
    cout << "Output: " << solution.convert(s2, numRows2) << endl;
    cout << "Expected: PINALSIGYAHRPI" << endl;

    string s3 = "AB";
    int numRows3 = 1;
    cout << "\nInput: " << s3 << ", numRows = " << numRows3 << endl;
    cout << "Output: " << solution.convert(s3, numRows3) << endl;
    cout << "Expected: AB" << endl;

    return 0;
}
