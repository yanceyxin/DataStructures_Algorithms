/*
leetocode 392：判断子序列
给定字符串 s 和 t ，判断 s 是否为 t 的子序列。

字符串的一个子序列是原始字符串删除一些（也可以不删除）字符而不改变剩余字符相对位置形成的新字符串。（例如，"ace"是"abcde"的一个子序列，而"aec"不是）。

进阶：

如果有大量输入的 S，称作 S1, S2, ... , Sk 其中 k >= 10亿，你需要依次检查它们是否为 T 的子序列。在这种情况下，你会怎样改变代码？

致谢：

特别感谢 @pbrother 添加此问题并且创建所有测试用例。

 

示例 1：

输入：s = "abc", t = "ahbgdc"
输出：true
示例 2：

输入：s = "axc", t = "ahbgdc"
输出：false
 

提示：

0 <= s.length <= 100
0 <= t.length <= 10^4
两个字符串都只由小写字符组成。
*/
#include <iostream>
#include <string>

bool isSubsequence(std::string s, std::string t) {
    int i = 0, j = 0;

    // 遍历 t，检查 s 中的每个字符是否按顺序出现
    while (i < s.size() && j < t.size()) {
        if (s[i] == t[j]) {
            i++; // 如果字符匹配，移动 s 的指针
        }
        j++; // 始终移动 t 的指针
    }

    // 如果 s 的所有字符都被匹配，则 i == s.size()
    return i == s.size();
}

int main() {
    std::string s = "abc";
    std::string t = "ahbgdc";

    if (isSubsequence(s, t)) {
        std::cout << "\"" << s << "\" is a subsequence of \"" << t << "\"" << std::endl;
    } else {
        std::cout << "\"" << s << "\" is NOT a subsequence of \"" << t << "\"" << std::endl;
    }

    return 0;
}
