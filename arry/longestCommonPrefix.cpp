/*
leetcode 14: 最长公共前缀

编写一个函数来查找字符串数组中的最长公共前缀。

如果不存在公共前缀，返回空字符串 ""。

 

示例 1：

输入：strs = ["flower","flow","flight"]
输出："fl"
示例 2：

输入：strs = ["dog","racecar","car"]
输出：""
解释：输入不存在公共前缀。
 

提示：

1 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] 仅由小写英文字母组成

解题思路：
1. 横向扫描：
取第一个字符串作为公共前缀，然后依次和其他字符串比较更新公共前缀。
如果某个字符串的公共前缀为空，则直接返回空字符串。

*/
#include <iostream>
#include <vector>
#include <string>
using namespace std;

string longestCommonPrefix(vector<string>& strs) {
    if (strs.empty()) return "";
    
    // 初始公共前缀为第一个字符串
    string prefix = strs[0];
    
    // 从第二个字符串开始，遍历所有字符串
    for (int i = 1; i < strs.size(); ++i) {
        
        // 不断缩小公共前缀的长度，使用 find(prefix) 函数，如果返回值不是 0，
        //表示当前字符串不以 prefix 为前缀，那么就将 prefix 缩短一个字符（即去掉最后一个字符），直到找到匹配为止。
        while (strs[i].find(prefix) != 0) {

            //substr() 函数用于提取一个子字符串，第一个参数是起始位置（0 表示从字符串的开头开始），第二个参数是子字符串的长度。
            prefix = prefix.substr(0, prefix.size() - 1); // 去掉最后一个字符
            if (prefix == "") return ""; // 如果公共前缀为空，直接返回空字符串
        }
    }
    
    return prefix;
}

int main() {
    vector<string> strs = {"flower", "flow", "flight"};
    cout << "Longest Common Prefix: " << longestCommonPrefix(strs) << endl;
    
    vector<string> strs2 = {"dog", "racecar", "car"};
    cout << "Longest Common Prefix: " << longestCommonPrefix(strs2) << endl;
    
    return 0;
}
