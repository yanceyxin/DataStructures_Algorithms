/*
leetcode 42: https://leetcode.cn/problems/trapping-rain-water/?envType=study-plan-v2&envId=top-interview-150

题目描述：
给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水。

 

示例 1：

输入：height = [0,1,0,2,1,0,1,3,2,1,2,1]
输出：6
解释：上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的高度图，在这种情况下，可以接 6 个单位的雨水（蓝色部分表示雨水）。 

示例 2：
输入：height = [4,2,0,3,2,5]
输出：9
 

提示：

n == height.length
1 <= n <= 2 * 10^4
0 <= height[i] <= 10^5

解法：
1. 额外数组法：
可以通过两个关键的概念来帮助解决这个问题：

左边的最大高度：对于每个柱子，其左侧的柱子中最高的一个是它能接到的最大高度。
右边的最大高度：对于每个柱子，其右侧的柱子中最高的一个也是它能接到的最大高度。

每个柱子上能接的水量是由它左边和右边的最大高度决定的，具体的计算公式是：
接水量=max(min(左边最大高度,右边最大高度)−当前柱子高度,0)

2. 双指针法：
*/

#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

int trap(vector<int>& height) {
    int n = height.size();
    if (n == 0) return 0;

    // 创建数组，分别存储左边和右边的最大高度
    vector<int> left_max(n), right_max(n);

    // 填充 left_max 数组
    left_max[0] = height[0];
    for (int i = 1; i < n; i++) {
        left_max[i] = max(left_max[i - 1], height[i]);
    }

    // 填充 right_max 数组
    right_max[n - 1] = height[n - 1];
    for (int i = n - 2; i >= 0; i--) {
        right_max[i] = max(right_max[i + 1], height[i]);
    }

    // 计算雨水量
    int water_trapped = 0;
    for (int i = 0; i < n; i++) {
        water_trapped += max(min(left_max[i], right_max[i]) - height[i], 0);
    }

    return water_trapped;
}

int trap2(vector<int>& height) {
    int n = height.size();
    if (n == 0) return 0;

    int left = 0, right = n - 1;
    int left_max = height[left], right_max = height[right];
    int water_trapped = 0;

    while (left < right) {
        if (height[left] < height[right]) {
            left++;
            left_max = max(left_max, height[left]);//左侧最大值
            water_trapped += max(0, left_max - height[left]);//最大值减去当前值即为左侧可盛水量
        } else {
            right--;
            right_max = max(right_max, height[right]);
            water_trapped += max(0, right_max - height[right]);
        }
    }

    return water_trapped;
}

int main() {
    vector<int> height = {2,1,0,2,1};
    cout << "Trapped water: " << trap2(height) << endl;  
    return 0;
}
