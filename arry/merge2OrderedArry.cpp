/*
 leetcode 88: https://leetcode.cn/problems/merge-sorted-array/description/?envType=study-plan-v2&envId=top-interview-150

 题目简述：
 两个非递减排序的数组nums1、nums2，另外两个整数m、n，分别表示数组nums1、nums2中元素的数目；
 请合并 nums2 到 nums1 中，合并后的数组仍然按照非递减排序。

 最终合并后数组不应该由函数返回，而是应该存在数组 nums1 中，因此nums1的初始长度应该为 m+n，
 其中前 m 个元素表示应合并的元素，后 n 个元素为 0 ，应忽略。nums2 的长度为 n 。

 示例：
 输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
 输出：[1,2,2,3,5,6]
 解释：需要合并 [1,2,3] 和 [2,5,6] 。
 合并结果是 [1,2,2,3,5,6] ，其中斜体加粗标注的为 nums1 中的元素。

 提示：
nums1.length == m + n
nums2.length == n
0 <= m, n <= 200
1 <= m + n <= 200
-10^9 <= nums1[i], nums2[j] <= 10^9 

思路：
1. 解法1：直接合并再排序, 时间复杂度: 合并O(n)；排序（快排）O(m+n)log(m+n)
2. 解法2：双指针法, 时间复杂度O(m+n)

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

//合并排序法
class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        for(int i = 0; i < n; i++){
            nums1[m+i] = nums2[i];
        }
        sort(nums1.begin(), nums1.end());
        
    }
};

//双指针法
class Solution2 {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n){
        int p1 = 0;
        int p2 = 0;
        int sorted[m+n];
        int cur;
        while(p1 < m || p2 < n){
            if(p1 == m){
                cur = nums2[p2++]; //nums1处理完，则处理nums2
            }
            else if(p2 == n){
                cur = nums1[p1++]; //nums2处理完，则处理nums1
            }
             //nums1和nums2都有元素，则比较二者大小
            else if(nums1[p1] < nums2[p2]){
                cur = nums1[p1++];

            }
            else {
                cur = nums2[p2++];
            }

            sorted[p1+p2-1] = cur;

        }
        for(int num = 0; num < m+n; num++){
            nums1[num] = sorted[num];
            
        }

    }
};
/*
 推理：
 nums1[6] = {1, 2, 3, 0, 0, 0}
 nums2[3] = {2, 5, 6};
 m=6-3=3, n=3

 p1=0,p2=0, sorteed[6]
 
 第一次while
 while(P1 < 3 || P2 <3)
    if(p1 == 3)/不成立
    else if(p2 == 3) //不成立
    else if(nums1[p1] < nums2[p2]) //1<2,成立
        cur = nums1[P1] /P1=0
        P1++
    else //不成立

    sorted[P1+P2-1] = sorted[1+0-1] = sorted[0] = cur = 1

 第二次while
while(P1 < 3 || P2 <3)
    if(p1 == 3)/不成立
    else if(p2 == 3) //不成立
    else if(nums1[p1] < nums2[p2]) //2<2,不成立

    else //成立
        cur = nums2[p2] //p2=0
        p2++

    sorted[P1+P2-1] = sorted[1+1-1] = sorted[1] = cur = 2

 第三次while
while(P1 < 3 || P2 <3)
    if(p1 == 3)/不成立
    else if(p2 == 3) //不成立
    else if(nums1[p1] < nums2[p2]) //2<5,成立
        cur = nums1[P1] /P1=1
        P1++
    else //不成立

    sorted[P1+P2-1] = sorted[2+1-1] = sorted[2] = cur = 2
以此类推。。。


*/



int main() {
    vector<int> nums1 = {1, 2, 3, 0, 0, 0};
    vector<int> nums2 = {2, 5, 6};

    int m = nums1.size() - 3;
    int n = nums2.size();

    Solution solution2;

    solution2.merge(nums1, m, nums2, n);

    cout << "Merged array: " << endl;
    for(int i = 0; i < nums1.size(); i++) {
        cout << "nums1[" << i << "]: " << nums1[i] << endl;
    }
    cout << endl;

    

    return 0;

}