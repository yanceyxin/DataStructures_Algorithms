/*
leetcode 11. 盛最多水的容器
给定一个长度为 n 的整数数组 height 。有 n 条垂线，第 i 条线的两个端点是 (i, 0) 和 (i, height[i]) 。

找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。

返回容器可以储存的最大水量。

说明：你不能倾斜容器。

输入：[1,8,6,2,5,4,8,3,7]
输出：49 
解释：图中垂直线代表输入数组 [1,8,6,2,5,4,8,3,7]。在此情况下，容器能够容纳水（表示为蓝色部分）的最大值为 49。
示例 2：

输入：height = [1,1]
输出：1
 

提示：

n == height.length
2 <= n <= 105
0 <= height[i] <= 10^4

*/
#include <iostream>
#include <vector>
#include <algorithm> // For std::min
using namespace std;

// Function to find the maximum area
int maxArea(vector<int>& height) {
    int left = 0;                  // Left pointer
    int right = height.size() - 1; // Right pointer
    int max_area = 0;              // To store the maximum area

    while (left < right) {
        // Calculate the current area
        int width = right - left;
        int current_area = min(height[left], height[right]) * width; //当前面积
        max_area = max(max_area, current_area);//更新最大面积

        // Move the pointer of the shorter line inward 移动指针：短板的高度决定了当前面积的上限
        if (height[left] < height[right]) {
            ++left;
        } else {
            --right;
        }
    }

    return max_area;
}

int main() {
    // Example input
    vector<int> height = {1, 8, 6, 2, 5, 4, 8, 3, 7};

    // Find the maximum water area
    int result = maxArea(height);

    // Output the result
    cout << "The maximum water area is: " << result << endl;

    // Additional test cases
    vector<vector<int>> testCases = {
        {1, 1},
        {4, 3, 2, 1, 4},
        {1, 2, 1},
        {1, 2, 4, 3, 6, 5, 8, 2},
    };

    for (const auto& testCase : testCases) {
        int testResult = maxArea(const_cast<vector<int>&>(testCase));
        cout << "For height: [ ";
        for (int h : testCase) {
            cout << h << " ";
        }
        cout << "] Max area: " << testResult << endl;
    }

    return 0;
}
