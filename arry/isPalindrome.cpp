/*
leetcode 125：验证回文串
如果在将所有大写字符转换为小写字符、并移除所有非字母数字字符之后，短语正着读和反着读都一样。则可以认为该短语是一个 回文串 。

字母和数字都属于字母数字字符。

给你一个字符串 s，如果它是 回文串 ，返回 true ；否则，返回 false 。

 

示例 1：

输入: s = "A man, a plan, a canal: Panama"
输出：true
解释："amanaplanacanalpanama" 是回文串。
示例 2：

输入：s = "race a car"
输出：false
解释："raceacar" 不是回文串。
示例 3：

输入：s = " "
输出：true
解释：在移除非字母数字字符之后，s 是一个空字符串 "" 。
由于空字符串正着反着读都一样，所以是回文串。
 

提示：

1 <= s.length <= 2 * 10^5
s 仅由可打印的 ASCII 字符组成
*/
#include <iostream>
#include <string>
#include <cctype> // 用于isalnum()
using namespace std;

class Solution {
public:
    bool isPalindrome(string s) {
        int left = 0, right = s.size() - 1;
        
        while (left < right) {
            // 跳过非字母和数字字符
            if (!isalnum(s[left])) {
                left++;
                continue;
            }
            if (!isalnum(s[right])) {
                right--;
                continue;
            }
            
            // 比较字符（忽略大小写）
            if (tolower(s[left]) != tolower(s[right])) {
                return false;
            }
            
            // 移动指针
            left++;
            right--;
        }
        
        return true;
    }
};

int main() {
    Solution sol;
    
    // 测试1
    string test1 = "A man, a plan, a canal: Panama";
    cout << "Test 1: " << test1 << endl;
    cout << "Is palindrome? " << (sol.isPalindrome(test1) ? "Yes" : "No") << endl;
    
    // 测试2
    string test2 = "race a car";
    cout << "Test 2: " << test2 << endl;
    cout << "test2 size: " << test2.size() << endl;
    cout << "Is palindrome? " << (sol.isPalindrome(test2) ? "Yes" : "No") << endl;
    
    // 测试3
    string test3 = " ";
    cout << "Test 3: " << test3 << endl;
    cout << "Is palindrome? " << (sol.isPalindrome(test3) ? "Yes" : "No") << endl;
    
    // 测试4
    string test4 = "ab_a";
    cout << "Test 4: " << test4 << endl;
    cout << "Is palindrome? " << (sol.isPalindrome(test4) ? "Yes" : "No") << endl;
    
    return 0;
}
