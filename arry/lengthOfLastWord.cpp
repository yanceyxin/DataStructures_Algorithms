/*
leetcode 58: 最后一个单词长度

给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中 最后一个 单词的长度。

单词 是指仅由字母组成、不包含任何空格字符的最大
子字符串
。
 

示例 1：
输入：s = "Hello World"
输出：5
解释：最后一个单词是“World”，长度为 5。

示例 2：
输入：s = "   fly me   to   the moon  "
输出：4
解释：最后一个单词是“moon”，长度为 4。

示例 3：
输入：s = "luffy is still joyboy"
输出：6
解释：最后一个单词是长度为 6 的“joyboy”。
 

提示：

1 <= s.length <= 104
s 仅有英文字母和空格 ' ' 组成
s 中至少存在一个单词
*/

#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int lengthOfLastWord(string s) {
    // 去除两端的空格
    int n = s.length();
    int end = n - 1;
    
    // 找到最后一个非空字符的下标
    while (end >= 0 && s[end] == ' ') {
        end--;
    }
    
    // 从后向前找到第一个空格
    int start = end;
    while (start >= 0 && s[start] != ' ') {
        start--;
    }
    
    // 返回最后一个单词的长度
    return end - start;
}

int main() {
    // 测试案例
    cout << lengthOfLastWord("Hello World") << endl; // 输出: 5
    cout << lengthOfLastWord("   fly me   to   the moon  ") << endl; // 输出: 4
    cout << lengthOfLastWord("luffy is still joyboy") << endl; // 输出: 6
    cout << lengthOfLastWord("a ") << endl; // 输出: 1

    return 0;
}
