/*
leetcode 26: https://leetcode.cn/problems/remove-duplicates-from-sorted-array/?envType=study-plan-v2&envId=top-interview-150
题目简述：
给你一个 非严格递增排列 的数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。元素的 相对顺序 应该保持 一致 。然后返回 nums 中唯一元素的个数。

考虑 nums 的唯一元素的数量为 k ，你需要做以下事情确保你的题解可以被通过：

更改数组 nums ，使 nums 的前 k 个元素包含唯一元素，并按照它们最初在 nums 中出现的顺序排列。nums 的其余元素与 nums 的大小不重要。
返回 k 。
判题标准:

系统会用下面的代码来测试你的题解:

int[] nums = [...]; // 输入数组
int[] expectedNums = [...]; // 长度正确的期望答案

int k = removeDuplicates(nums); // 调用

assert k == expectedNums.length;
for (int i = 0; i < k; i++) {
    assert nums[i] == expectedNums[i];
}
如果所有断言都通过，那么您的题解将被 通过。

 

示例 1：

输入：nums = [1,1,2]
输出：2, nums = [1,2,_]
解释：函数应该返回新的长度 2 ，并且原数组 nums 的前两个元素被修改为 1, 2 。不需要考虑数组中超出新长度后面的元素。
示例 2：

输入：nums = [0,0,1,1,1,2,2,3,3,4]
输出：5, nums = [0,1,2,3,4]
解释：函数应该返回新的长度 5 ， 并且原数组 nums 的前五个元素被修改为 0, 1, 2, 3, 4 。不需要考虑数组中超出新长度后面的元素。
 

提示：

1 <= nums.length <= 3 * 10^4
-104 <= nums[i] <= 104
nums 已按 非严格递增 排列


解题思路：
1. 双指针：快慢指针同时指向索引 1，前后不相同，快慢指针都向前走一步，前后相同，慢指针不走快指针走一步；时间复杂度 O(n)
*/

//=================================2222=================================//
/*
leetcode 80：https://leetcode.cn/problems/remove-duplicates-from-sorted-array-ii/description/?envType=study-plan-v2&envId=top-interview-150

题目描述：
给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使得出现次数超过两次的元素只出现两次 ，返回删除后数组的新长度。

不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。

 

说明：

为什么返回数值是整数，但输出的答案是数组呢？

请注意，输入数组是以「引用」方式传递的，这意味着在函数里修改输入数组对于调用者是可见的。

你可以想象内部操作如下:

// nums 是以“引用”方式传递的。也就是说，不对实参做任何拷贝
int len = removeDuplicates(nums);

// 在函数里修改输入数组对于调用者是可见的。
// 根据你的函数返回的长度, 它会打印出数组中 该长度范围内 的所有元素。
for (int i = 0; i < len; i++) {
    print(nums[i]);
}
 

示例 1：
输入：nums = [1,1,1,2,2,3]
输出：5, nums = [1,1,2,2,3]
解释：函数应返回新长度 length = 5, 并且原数组的前五个元素被修改为 1, 1, 2, 2, 3。 不需要考虑数组中超出新长度后面的元素。

示例 2：
输入：nums = [0,0,1,1,1,1,2,3,3]
输出：7, nums = [0,0,1,1,2,3,3]
解释：函数应返回新长度 length = 7, 并且原数组的前七个元素被修改为 0, 0, 1, 1, 2, 3, 3。不需要考虑数组中超出新长度后面的元素。

提示：
1 <= nums.length <= 3 * 10^4
-10^4 <= nums[i] <= 10^4
nums 已按升序排列

解题思路：
1. 双指针法：检查上上个应该被保留的元素与当前待检查元素是否相等，相等则不保留下来，不相等则保留下来，以慢指针为保留下来基准，
   快慢指针初始化指向序号为2；时间复杂度为O(n)

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int removeDuplicates(vector<int>& nums)  {
        int n = nums.size();
        if (n == 0) {
            return 0;
        }
        int fast = 1, slow = 1;
        
        while (fast < n) {
            if (nums[fast] != nums[fast - 1]) {
                nums[slow] = nums[fast];
                ++slow;
            }
            ++fast;
        }
        nums.resize(slow);

        return slow;
    }
};

/*
推演：
  s
  f
1 1 2   此时 nums[0]=0 ,nums[1]=1 ,f向后移动一位 f=2
  
  s
    f
1 1 2  此时 nums[1] = nums[2]=2，s 向后移动一位，s=2，f 向后移动一位，f=3

while 不成立，结束


nums = 1 1 2
n = 3

fast=1,slow=1
while 1<3
    if nums[1] != nums[0] 不成立

    ++fast fast=2

while 2<3
    if nums[2] != nums[1] 成立
        nums[1] = fast[2]
        ++slow slow=2
    ++fast fast = 3

while 3<3 不成立

return slow =2
*/

class Solution2 {
public:
    int removeDuplicates(vector<int>& nums) {
        int n = nums.size();
        if (n <= 2) {
            return n;
        }
        int slow = 2, fast = 2;
        while (fast < n) {
            if (nums[slow - 2] != nums[fast]) {
                nums[slow] = nums[fast];
                ++slow;
            }
            ++fast;
        }
        nums.resize(slow);
        return slow;
    }
};

/*
推演：
    f
    s
0 0 1 1 1 1 2 3 3 进入if，nums[2]=nums[2]=1,s=3 f=3

      f
      s
0 0 1 1 1 1 2 3 3 进入if，nums[3]=nums[3]=1,s=4 f=4

        f
        s
0 0 1 1 1 1 2 3 3 不进入if，s=4,f=5

          f
        s
0 0 1 1 1 1 2 3 3 不进入if, s=4 f=6

            f
        s
0 0 1 1 1 1 2 3 3 进入if，nums[4]=nums[6]=2,s=5 f=7

              f
          s
0 0 1 1 1 1 2 3 3 进入if，nums[5]=nums[7]=3,s=6 f=8

                f
            s
0 0 1 1 1 1 2 3 3 进入if，nums[6]=nums[8]=3,s=7 f=9

while条件不满足，退出
nums.resize(s=7)
return s=7
*/


int main() {
    vector <int> nums = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4}; //数组必须是非严格递增，该算法才有效，若是无序数组，可以先排序sort下再移除。
    Solution solution;
    int number =  solution.removeDuplicates(nums);

    cout << "number: " << number << endl;
    cout << "nums.size: " << nums.size() << endl;

    for(int i = 0; i < nums.size(); i++){
        cout << "i: " << i << "; nums[" << i << "]: " << nums[i] << endl;

    }

    //验证2
    vector <int> nums2 = {0, 0, 1, 1, 1, 1, 2, 3, 3};//非严格递增，若是无序则排序后再移除重复元素
    Solution2 sloution2;
    int number2 = sloution2.removeDuplicates(nums2);
    
    cout << "number2: " << number2 << endl;
    cout << "nums2.size: " << nums2.size() << endl;

    for(int i = 0; i < nums2.size(); i++){
        cout << "i: " << i << "; nums2[" << i << "]: " << nums2[i] << endl;

    }

    return 0;
}