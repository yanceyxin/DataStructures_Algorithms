/*
leetcode 48. 旋转图像
给定一个 n × n 的二维矩阵 matrix 表示一个图像。请你将图像顺时针旋转 90 度。

你必须在 原地 旋转图像，这意味着你需要直接修改输入的二维矩阵。请不要 使用另一个矩阵来旋转图像。

 

示例 1：


输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
输出：[[7,4,1],[8,5,2],[9,6,3]]
示例 2：


输入：matrix = [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
输出：[[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]
 

提示：

n == matrix.length == matrix[i].length
1 <= n <= 20
-1000 <= matrix[i][j] <= 1000
 */
#include <vector>
#include <iostream>
using namespace std;

void rotate(vector<vector<int>>& matrix) {
    int n = matrix.size();
    
    // Step 1: 转置矩阵
    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n; ++j) {
            swap(matrix[i][j], matrix[j][i]);
        }
    }
    
    // Step 2: 反转每一行
    for (int i = 0; i < n; ++i) {
        int left = 0, right = n - 1;
        while (left < right) {
            swap(matrix[i][left], matrix[i][right]);
            ++left;
            --right;
        }
    }
}

int main() {
    vector<vector<int>> matrix = {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };
    
    rotate(matrix);
    
    // 输出旋转后的矩阵
    for (const auto& row : matrix) {
        for (int elem : row) {
            cout << elem << " ";
        }
        cout << endl;
    }
    
    return 0;
}

/*
假设我们有一个 3x3 的矩阵：

原矩阵：
[
 [1, 2, 3],
 [4, 5, 6],
 [7, 8, 9]
]

步骤 1: 转置矩阵
转置操作的意思是将矩阵的行和列交换。具体来说，我们交换 matrix[i][j] 和 matrix[j][i]，其中 i 和 j 互换。

原矩阵转置的过程：

matrix[0][1] 和 matrix[1][0] 交换；
matrix[0][2] 和 matrix[2][0] 交换；
matrix[1][2] 和 matrix[2][1] 交换。
矩阵转置后：

[
 [1, 4, 7],
 [2, 5, 8],
 [3, 6, 9]
]

步骤 2: 反转每一行
反转每一行的操作就是将每行的元素顺序倒过来。对于每一行 i，我们将 matrix[i][left] 和 matrix[i][right] 交换，其中 left 指向当前行的左端，right 指向右端。

反转每一行的过程：

对于第一行 [1, 4, 7]，交换 matrix[0][0] 和 matrix[0][2]，结果是 [7, 4, 1]。
对于第二行 [2, 5, 8]，交换 matrix[1][0] 和 matrix[1][2]，结果是 [8, 5, 2]。
对于第三行 [3, 6, 9]，交换 matrix[2][0] 和 matrix[2][2]，结果是 [9, 6, 3]。
反转后的矩阵：


[
 [7, 4, 1],
 [8, 5, 2],
 [9, 6, 3]
]
*/