/*
leetcode 189：https://leetcode.cn/problems/rotate-array/description/?envType=study-plan-v2&envId=top-interview-150

题目描述：
给定一个整数数组 nums，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。

示例 1:
输入: nums = [1,2,3,4,5,6,7], k = 3
输出: [5,6,7,1,2,3,4]
解释:
向右轮转 1 步: [7,1,2,3,4,5,6]
向右轮转 2 步: [6,7,1,2,3,4,5]
向右轮转 3 步: [5,6,7,1,2,3,4]

示例 2:
输入：nums = [-1,-100,3,99], k = 2
输出：[3,99,-1,-100]
解释: 
向右轮转 1 步: [99,-1,-100,3]
向右轮转 2 步: [3,99,-1,-100]
 

提示：
1 <= nums.length <= 10^5
-2^31 <= nums[i] <= 2^31 - 1
0 <= k <= 10^5
 
进阶：

尽可能想出更多的解决方案，至少有 三种 不同的方法可以解决这个问题。
你可以使用空间复杂度为 O(1) 的 原地 算法解决这个问题吗？

解题思路：
1. 额外数组法：遍历原数组，将原数组下标为 i 的元素放至新数组下标为 (i+k) mod n 的位置，最后将新数组拷贝至原数组即可。时间复杂度O（n）
2. 原数组旋转法：



*/
#include <iostream>
#include <vector>

using namespace std;


class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int n = nums.size();
        vector <int> newarr(n);
        for(int i = 0; i < n; i++ ) {
            newarr[(i+k)%n] = nums[i];
        }

        nums.assign(newarr.begin(), newarr.end());  
    }
};

/*
 推演：
 n=7,k=3
 (0+3)%7=3 newarr[3] = nums[0] 1
 (1+3)%7=4 newarr[4] = nums[1] 2
 (2+3)%7=5 newarr[5] = nums[2] 3
 (3+3)%7=6 newarr[6] = nums[3] 4
 (4+3)%7=0 newarr[0] = nums[4] 5
 (5+3)%7=1 newarr[1] = nums[5] 6
 (6+3)%7=2 newarr[2] = nums[6] 7
*/
class Solution2 {
public:
    void reverse(vector<int>& nums, int start, int end) {
        while (start < end) {
            swap(nums[start], nums[end]);
            start += 1;
            end -= 1;
        }
    }
    void rotate(vector<int>& nums, int k) {
        k %= nums.size(); //k=3%7=3,为了旋转不超过数组长度
        reverse(nums, 0, nums.size() - 1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, nums.size() - 1);
    }
};
/*
推演:
 1 2 3 4 5 6 7
 7 6 5 4 3 2 1
 5 6 7 4 3 2 1
 5 6 7 1 2 3 4

*/

int main() {
    vector <int> nums = {1, 2, 3, 4, 5, 6, 7};
    int k = 3;
    Solution2 solution;
    solution.rotate(nums, k);
    for(int i = 0; i < nums.size(); i++) {
        cout << "nums[" << i << "]: " << nums[i] << endl;
    }

    return 0;
}
