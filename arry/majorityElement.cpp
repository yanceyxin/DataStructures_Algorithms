/*
leetcode 169: https://leetcode.cn/problems/majority-element/?envType=study-plan-v2&envId=top-interview-150

题目描述：
给定一个大小为 n 的数组 nums ，返回其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。

你可以假设数组是非空的，并且给定的数组总是存在多数元素。

示例 1：
输入：nums = [3,2,3]
输出：3

示例 2：
输入：nums = [2,2,1,1,1,2,2]
输出：2

提示：
n == nums.length
1 <= n <= 5 * 10^4
-10^9 <= nums[i] <= 10^9
 
进阶：尝试设计时间复杂度为 O(n)、空间复杂度为 O(1) 的算法解决此问题。


解题思路：
1. Boyer-Moore投票算法: 时间复杂度 O(n), 空间复杂度 O(1);Boyer-Moore投票算法适用于解决“多数元素问题”，
                       即在一个序列中找出一个出现次数超过n/2的元素（如果存在的话）
2. 排序法：先排序，下标n/2的位置一定是多数元素，复杂度根据排序算法的复杂度来

*/
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int count = 0;
        int candidate = 0;
        for(int i = 0; i < nums.size(); i++){
            if(count == 0){
                candidate = nums[i];
            }

            count += (candidate == nums[i]) ? 1 : -1;
        }
        
        return candidate;
    }
};

//排序法
class Solution2 {
public:
    int majorityElement(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        return (nums[nums.size()/2]);
    }
};



int  main(){

    vector <int> nums = {3, 2, 3};
    Solution2 solution;

    int element = solution.majorityElement(nums);

    cout << "major elemet: " << element << endl;

    return 0;
}