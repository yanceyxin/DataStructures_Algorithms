#include <iostream>
using namespace std;
 
// 定义结构体用于示例 
struct Student {
    int age;
    string name;
};
 

int main() {
    // 1. 基础变量指针 
    int a = 10;
    int* p1 = &a;
    *p1 = 20;
    cout << "基础指针: " << a << endl; // 输出 20 [7]()
 
    // 2. 空指针与野指针 
    int* p2 = nullptr; // 安全空指针 
    // int* p3;        // 未初始化野指针（注释避免运行时崩溃）
 
    // 3. 数组指针操作 
    int arr[] = {1,2,3,4,5};
    int* p4 = arr;
    cout << "数组指针: " << *(p4 + 2) << endl; // 输出 3 [3]()
 
    // 4. 字符串指针 
    const char* str = "Hello";
    cout << "字符串指针: " << str[1]  << endl; // 输出 'e' [6]()
 
    // 5. 结构体指针 
    Student s = {20, "Tom"};
    Student* p5 = &s;
    cout << "结构体指针: " << p5->name << endl; // 输出 Tom [5]()
 
    // 6. 动态内存分配 
    int* p6 = new int(100);
    cout << "动态内存: " << *p6 << endl;
    delete p6; // 必须释放 [7]()
 
    // 7. 多维数组指针 
    int matrix[2][3] = {{1,2,3}, {4,5,6}};
    int (*p7)[3]  = matrix;
    cout << "多维数组: " << p7[1][2] << endl; // 输出 6 [3]()
 
    // 8. 函数指针 
    int (*funcPtr)(int, int) = [](int x, int y) { return x + y; };
    cout << "函数指针: " << funcPtr(3,5) << endl; // 输出 8 [6]()
 
    return 0;
}