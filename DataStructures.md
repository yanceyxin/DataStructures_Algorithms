# 数据结构

1. **定义**：数据结构是一种数据组织、管理和存储的格式。它是相互之间存在一种或多种特定关系的数据元素的集合。
2. **一句话概括总结理解**：各种数据结构，都是数组（顺序存储）和链表（链式存储）的变换或者称之为变种。
3. **数据结构的关键点**：数据结构的关键点就是遍历和访问，即增加、删除、查找、修改等基本操作。

## 数据结构的存储方式

1. **数据结构的存储方式只有两种**：顺序存储（数组）、链式存储（链表）。
2. 所以说数组和链表是数据结构的基础数据、 结构基础，像其他的哈希表、队列、树、堆、栈、图等数据结构都是在数组或链表上的特殊操作，就相当于封装了 API。

### 顺序存储

1. **定义**：将数据元素按照一定的顺序存放在连续的存储单元中，是实现数据结构的一种基础方式；数组是顺序存储结构的一个典型应用，此外字符串也可以理解成按照顺序存储的方式。
2. 在计算机中，顺序存储通常通过数组来实现。数组提供了一种机制，使得数据元素可以按照一定的顺序存放在连续的内存地址中。

### 链式存储

1. **定义**：通过指针或引用将数据元素连接起来，形成一个链表。在链式存储中，每个数据元素通常包含两部分：一部分是存储数据本身，另一部分是一个或多个指向其他数据元素的指针或引用。链表是链式存储结构的一个典型应用。

### 两种存储方式的优缺点

链式存储和顺序存储是两种基本的数据存储方式，它们各自有不同的优缺点，适用于不同的应用场景。以下是它们的优缺点对比：

### 顺序存储的优缺点

**优点：**

1. **快速随机访问**：由于数据元素在内存中连续存放，可以通过计算偏移量快速访问任意位置的元素。
2. **空间利用率高**：顺序存储通常不需要额外的空间来存储指针或引用，空间利用率较高。
3. **缓存友好**：由于数据元素连续存放，顺序存储对缓存友好，可以提高数据访问效率。
4. **简单易实现**：顺序存储的实现相对简单，易于理解和操作。

**缺点：**

1. **动态扩展困难**：顺序存储通常需要预先分配固定大小的存储空间，当数据量超过初始分配的空间时，可能需要重新分配更大的空间，并将原有数据复制到新空间（但现在一些语言会解决数组动态扩展的困难，比如 c++ 的中的标准库 std::vector）。
2. **插入和删除操作可能较慢**：在顺序存储结构中，插入或删除元素可能需要移动大量元素来维持数据的连续性，这在某些情况下可能导致性能下降。
3. **内存浪费**：如果数组的大小固定，而实际存储的元素数量少于数组的大小，那么可能会造成内存的浪费。

### 链式存储的优缺点

**优点：**

1. **动态性**：链式存储的数据结构可以动态地增加或减少元素，不需要预先分配固定大小的存储空间。
2. **灵活性**：链式存储提供了高度的灵活性，可以轻松地在数据结构中插入或删除元素，而不需要移动其他元素。
3. **非连续性**：数据元素在内存中不需要连续存放，可以分散在内存的任何位置，通过指针相互连接。
4. **适合稀疏数据结构**：对于稀疏的数据结构，如稀疏矩阵，链式存储可以更有效地利用内存空间。

**缺点：**

1. **内存开销**：链式存储的每个数据元素都需要额外的空间来存储指针或引用，这可能导致更高的内存开销。
2. **不支持高效的随机访问**：要访问特定位置的元素，可能需要从头开始遍历链表，直到找到目标元素，这可能导致性能下降。
3. **缓存不友好**：由于数据元素不连续存放，链式存储可能不利于缓存的利用，影响数据访问效率。
4. **实现复杂度**：链式存储的实现相对复杂，需要管理指针或引用，增加了编程的复杂度。

## 数据结构基本操作

1. 对于任何数据结构，基本操作都是**遍历**和**访问**，具体一点即**增加**、**删除**、**查找**、**修改**等基本操作。
2. **数据结构的使命**：尽可能地高效完成**增删查修**的操作。
3. 数据结构遍历和访问的形式：线性（for/while 迭代等）和非线性（递归等）。

### 数组的遍历框架

1. 数组的遍历属于典型的线性迭代结构：

```cpp
void traverse(vector<int>& arr)
{
    for(int i : arr)//基于范围的 for 循环只能用在容器或数组
    {
        //遍历数组 arr[i]
    }
}

```

### 链表的遍历框架

1. 链表的遍历可以做到迭代和递归：

```cpp
// 基本的单链表节点
class ListNode {
    public:
        int val;
        ListNode* next;
};

void traverse(ListNode* head) {
    for (ListNode* p = head; p != nullptr; p = p->next) {
        // 迭代访问 p->val
    }
}

void traverse(ListNode* head) {
    // 递归访问 head->val
    traverse(head->next);
}
```

### 二叉树的遍历框架

1. 二叉树的遍历属于典型的非线性递归遍历结构：

```cpp
// 基本的二叉树节点
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
};

void traverse(TreeNode* root) {
    traverse(root->left);
    traverse(root->right);
}
```

# 数组 Array

1. **定义**：数组是一种基本的数据结构，用于存储**相同类型**的元素集合。在不同的编程语言中，数组的实现和特性可能略有不同，但基本概念是相似的。

## 数组的特性

1. **固定大小**：在静态数组中，一旦声明，其大小就不能改变。动态数组（如C++的std::vector或Java的ArrayList）可以在运行时调整大小。
2. **连续内存**：数组的元素通常在内存中连续存储，这使得访问元素的速度很快。
3. **随机访问**：可以通过索引快速访问数组中的任何元素。
4. **同质性**：数组中的所有元素都是相同类型的。

## 数组的基本操作

1. **访问元素**：使用索引访问数组中的元素，例如array[index]。
2. **遍历**：通过循环遍历数组中的所有元素。
3. **搜索**：在数组中搜索特定的元素或值。
4. **插入和删除**：在数组中插入或删除元素可能涉及移动其他元素以保持连续性，这在静态数组中可能是一个昂贵的操作。
5. **排序**：对数组中的元素进行排序，可以使用各种算法，如快速排序、归并排序等。

## 举例

```cpp
int arr[5] = {1, 2, 3, 4, 5}; // 静态数组
std::vector<int> vec = {1, 2, 3, 4, 5}; // 动态数组

// 访问元素
int element = arr[2]; // 访问第三个元素

// 遍历
for (int i = 0; i < 5; i++) {
    printf("%d ", arr[i]);
}
```

# 链表 Linked List

1. **定义**：链表是一种常见的基础数据结构，它由一系列节点组成，每个节点包含数据部分和指向下一个节点的指针（在双向链表中还会包含指向前一个节点的指针）。

## 链表的特性

1. **单链表（Singly Linked List）**

- 结构：每个节点包含数据和指向下一个节点的指针。
- 操作：插入和删除操作通常只需要改变指针，不需要移动数据。
- 遍历：只能从表头向表尾方向遍历。

2. **双向链表（Doubly Linked List）**

- 结构：每个节点包含数据和两个指针，一个指向前一个节点，一个指向后一个节点。
- 操作：可以轻松地从任一方向遍历链表，插入和删除操作也较为灵活。
- 遍历：可以从表头向表尾遍历，也可以从表尾向表头遍历。

3. **循环链表（Circular Linked List）**

- 结构：单链表或双向链表的变种，最后一个节点的指针指向第一个节点，形成一个闭环。
- 操作：可以在到达链表末尾后继续从开头遍历。
- 遍历：通常用于实现队列和栈等数据结构。

## 链表的操作

1. **动态大小**：链表的大小可以根据需要动态变化，不需要预先分配固定大小的内存空间。
2. **内存分配**：链表的每个节点通常在堆上分配，这样可以灵活地控制每个节点的大小。
3. **插入和删除**：在已知节点的情况下，插入和删除操作的时间复杂度为 O(1)，因为只需要改变指针。
4. **随机访问**：链表不支持高效的随机访问，访问任意位置的节点通常需要从头开始遍历，时间复杂度为 O(n)。

## 举例

```cpp
#include <iostream>
//定义链表节点
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {} //构造函数，也可以不在节点里写构造函数
};
//创建链表
ListNode* createList() {
    ListNode* head = new ListNode(1);
    head->next = new ListNode(2);
    head->next->next = new ListNode(3);
    head->next->next->next = new ListNode(4);
    head->next->next->next->next = new ListNode(5);
    return head;
}
//打印链表
void printList(ListNode* head) {
    ListNode* current = head;
    while (current != nullptr) {
        std::cout << current->val << " -> ";
        current = current->next;
    }
    std::cout << "nullptr" << std::endl;
}
//插入节点
void insertNode(ListNode*& head, int val, int position) {
    ListNode* newNode = new ListNode(val);
    if (position == 0) {
        newNode->next = head;
        head = newNode;
        return;
    }
    ListNode* current = head;
    for (int i = 0; current != nullptr && i < position - 1; i++) {
        current = current->next;
    }
    if (current == nullptr) {
        std::cout << "Position out of bounds" << std::endl;
        return;
    }
    newNode->next = current->next;
    current->next = newNode;
}
//删除节点
void deleteNode(ListNode*& head, int position) {
    if (head == nullptr) return;
    if (position == 0) {
        head = head->next;
        return;
    }
    ListNode* current = head;
    for (int i = 0; current != nullptr && i < position - 1; i++) {
        current = current->next;
    }
    if (current == nullptr || current->next == nullptr) {
        std::cout << "Position out of bounds" << std::endl;
        return;
    }
    ListNode* next = current->next->next;
    delete current->next;
    current->next = next;
}
//释放链表
void deleteList(ListNode* head) {
    while (head != nullptr) {
        ListNode* temp = head;
        head = head->next;
        delete temp;
    }
}
//主函数
int main() {
    ListNode* head = createList();
    std::cout << "Original List: ";
    printList(head);

    insertNode(head, 6, 3); // 在位置3插入值6
    std::cout << "List after inserting 6 at position 3: ";
    printList(head);

    deleteNode(head, 2); // 删除位置2的节点
    std::cout << "List after deleting node at position 2: ";
    printList(head);

    deleteList(head);

    return 0;
}
```

# 队列 Queue

1. **定义**：队列（Queue）是一种遵循先进先出（FIFO，First-In-First-Out）原则的数据结构，它允许在一端添加元素（称为队尾，rear），并在另一端移除元素（称为队头，front）。队列的两个主要操作是入队（enqueue），即在队尾添加一个元素，以及出队（dequeue），即从队头移除一个元素。

## 示例

```cpp
#include <iostream>
#include <list>

class Queue {
private:
    std::list<int> queue; // 使用list容器来存储队列中的元素

public:
    // 入队操作
    void enqueue(int value) {
        queue.push_back(value);
    }

    // 出队操作
    void dequeue() {
        if (isEmpty()) {
            std::cout << "Queue is empty. Cannot dequeue." << std::endl;
            return;
        }
        queue.pop_front();
    }

    // 获取队头元素
    int front() const {
        if (isEmpty()) {
            throw std::runtime_error("Queue is empty.");
        }
        return queue.front();
    }

    // 检查队列是否为空
    bool isEmpty() const {
        return queue.empty();
    }

    // 打印队列中的所有元素
    void print() const {
        std::list<int>::const_iterator it = queue.begin();
        std::cout << "Queue: ";
        while (it != queue.end()) {
            std::cout << *it << " ";
            ++it;
        }
        std::cout << std::endl;
    }
};

int main() {
    Queue q;

    // 入队元素
    q.enqueue(1);
    q.enqueue(2);
    q.enqueue(3);
    q.print(); // 打印队列

    // 出队元素
    q.dequeue();
    q.print(); // 打印队列

    // 获取队头元素
    std::cout << "Front element is: " << q.front() << std::endl;

    // 再次出队元素
    q.dequeue();
    q.print(); // 打印队列

    return 0;
}
```

# 栈 Stack

1. **定义**：栈是一种遵循**后进先出（LIFO）**原则的数据结构。
2. **基本操作**：push 和 pop。push 操作用于将元素添加到栈顶，而 pop 操作用于移除栈顶元素。栈的其他操作还包括查看栈顶元素（peek 或 top）而不移除它，检查栈是否为空（isEmpty），以及获取栈的大小（size）。

## 示例

```cpp
#include <iostream>
#include <vector>
#include <stdexcept> // 用于抛出异常

class Stack {
private:
    std::vector<int> elements; // 使用vector来存储栈中的元素

public:
    // 向栈中添加元素
    void push(int element) {
        elements.push_back(element);
    }

    // 移除栈顶元素并返回它
    int pop() {
        if (isEmpty()) {
            throw std::out_of_range("Stack<>::pop(): empty stack");
        }
        int top = elements.back();
        elements.pop_back();
        return top;
    }

    // 查看栈顶元素但不移除
    int peek() const {
        if (isEmpty()) {
            throw std::out_of_range("Stack<>::peek(): empty stack");
        }
        return elements.back();
    }

    // 检查栈是否为空
    bool isEmpty() const {
        return elements.empty();
    }

    // 获取栈的大小
    size_t size() const {
        return elements.size();
    }
};

int main() {
    Stack stack;

    // 向栈中添加元素
    stack.push(1);
    stack.push(2);
    stack.push(3);

    // 打印栈顶元素
    std::cout << "Top element is: " << stack.peek() << std::endl;

    // 移除并打印栈顶元素
    std::cout << "Popped element is: " << stack.pop() << std::endl;

    // 再次打印栈顶元素
    std::cout << "Top element is now: " << stack.peek() << std::endl;

    // 检查栈是否为空
    std::cout << "Is the stack empty? " << (stack.isEmpty() ? "Yes" : "No") << std::endl;

    // 打印栈的大小
    std::cout << "Stack size is: " << stack.size() << std::endl;

    return 0;
}
```

# 树 Tree

1. **定义**：树（Tree）是一种非线性数据结构，它由节点（Node）和边（Edge）组成，用来表示具有层次关系的数据集合。在树结构中，任意两个节点之间只有一条路径，并且树中不存在环。
2. **基本术语**：
   - 节点（Node）：树结构中的每一个元素称为节点，每个节点可以包含数据和指向其子节点的指针。
   - 根节点（Root）：树的顶级节点，没有父节点。
   - 子节点（Child）：一个节点的直接后继节点。
   - 父节点（Parent）：一个节点的直接前驱节点。
   - 叶子节点（Leaf）：没有子节点的节点。
   - 边（Edge）：连接父子节点之间的连线。
   - 路径（Path）：从根节点到任意节点的节点序列。
   - 深度（Depth）：从根节点到该节点的边的数量。
   - 高度（Height）：树的高度是从根节点到叶子节点的最长路径上的边数。
   - 兄弟节点（Sibling）：具有相同父节点的节点。
   - 子树（Subtree）：以某个节点为根的树，该节点可以是任意节点。
3. **树的类型**：
   - 二叉树（Binary Tree）：每个节点最多有两个子节点的树，通常称为左子节点和右子节点。
   - 平衡树（Balanced Tree）：保持树的平衡，以确保操作（如查找、插入和删除）的时间复杂度为O(log n)。
   - 搜索树（Search Tree）：节点的键值满足左子树的键值小于根节点，右子树的键值大于根节点的树。
   - B树（B-Tree）：一种多路搜索树，用于数据库和文件系统中，可以有多个子节点。
   - B+树（B+ Tree）：B树的变种，所有值都在叶子节点中出现，并且叶子节点之间通过指针连接。
   - 前缀树（Trie）：用于存储关联数组，其中键通常是字符串。
   - 后缀树（Suffix Tree）：用于处理字符串的后缀，常用于文本搜索和模式匹配。
4. 树的操作：
   - 插入（Insert）：向树中添加新节点。
   - 删除（Delete）：从树中移除节点。
   - 查找（Search）：在树中查找特定值的节点。
   - 遍历（Traversal）：访问树中的每个节点，常见的遍历方式包括：
     - 前序遍历（Pre-order）：先访问根节点，然后递归遍历左子树，最后递归遍历右子树。
     - 中序遍历（In-order）：先递归遍历左子树，然后访问根节点，最后递归遍历右子树。
     - 后序遍历（Post-order）：先递归遍历左子树，然后递归遍历右子树，最后访问根节点。
     - 层序遍历（Level-order）：按照树的层次从上到下，从左到右访问每个节点。

## 二叉树 Binary Tree

1. **定义**：二叉树（Binary Tree）是一种特殊的树形数据结构，其中每个节点最多有两个子节点，通常称为左子节点和右子节点。这种结构因其简洁性和灵活性，在计算机科学中有着广泛的应用。
2. **二叉树类型**：
   - 完全二叉树（Complete Binary Tree）：除了最后一层外，每一层都被完全填满，并且所有节点都尽可能地靠左排列。
   - 满二叉树（Full Binary Tree）：所有节点都有0个或2个子节点，即每个节点要么是叶子节点，要么有两个子节点。
   - 平衡二叉树（Balanced Binary Tree）：任何两个叶子节点的深度差不超过1。
   - 二叉搜索树（Binary Search Tree, BST）：对于每个节点，其左子树中的所有节点的值都小于该节点的值，其右子树中的所有节点的值都大于该节点的值。
3. **二叉树的操作**：
   - 插入（Insert）：向二叉树中添加新节点。
   - 删除（Delete）：从二叉树中移除节点。
   - 查找（Search）：在二叉树中查找特定值的节点。
   - 遍历（Traversal）：访问二叉树中的每个节点，常见的遍历方式包括：
   - 前序遍历（Pre-order）：根节点 -> 左子树 -> 右子树。
   - 中序遍历（In-order）：左子树 -> 根节点 -> 右子树。
   - 后序遍历（Post-order）：左子树 -> 右子树 -> 根节点。
   - 层序遍历（Level-order）：按层次从上到下，从左到右访问每个节点。
4. **示例**：

```cpp
#include <iostream>
#include <queue>

// 定义二叉树节点结构体
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;

    // 构造函数
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

// 创建一个新的二叉树节点
TreeNode* createNode(int value) {
    return new TreeNode(value);
}

// 插入节点的辅助函数，用于递归插入
TreeNode* insertNode(TreeNode* root, int value) {
    if (root == nullptr) {
        return createNode(value);
    }

    if (value < root->val) {
        root->left = insertNode(root->left, value);
    } else if (value > root->val) {
        root->right = insertNode(root->right, value);
    }

    return root;
}

// 删除节点的辅助函数
TreeNode* deleteNode(TreeNode* root, int value) {
    if (root == nullptr) {
        return root;
    }

    if (value < root->val) {
        root->left = deleteNode(root->left, value);
    } else if (value > root->val) {
        root->right = deleteNode(root->right, value);
    } else {
        // Node with only one child or no child
        if (root->left == nullptr) {
            TreeNode* temp = root->right;
            delete root;
            return temp;
        } else if (root->right == nullptr) {
            TreeNode* temp = root->left;
            delete root;
            return temp;
        }

        // Node with two children: Get the inorder successor (smallest in the right subtree)
        TreeNode* temp = minValueNode(root->right);

        // Copy the inorder successor's content to this node
        root->val = temp->val;

        // Delete the inorder successor
        root->right = deleteNode(root->right, temp->val);
    }

    return root;
}

// Find the node with the minimum value in the tree
TreeNode* minValueNode(TreeNode* node) {
    TreeNode* current = node;
    while (current && current->left != nullptr) {
        current = current->left;
    }
    return current;
}

// 前序遍历二叉树
void preorderTraversal(TreeNode* root) {
    if (root != nullptr) {
        std::cout << root->val << " ";
        preorderTraversal(root->left);
        preorderTraversal(root->right);
    }
}

// 中序遍历二叉树
void inorderTraversal(TreeNode* root) {
    if (root != nullptr) {
        inorderTraversal(root->left);
        std::cout << root->val << " ";
        inorderTraversal(root->right);
    }
}

// 后序遍历二叉树
void postorderTraversal(TreeNode* root) {
    if (root != nullptr) {
        postorderTraversal(root->left);
        postorderTraversal(root->right);
        std::cout << root->val << " ";
    }
}

// 释放二叉树内存
void deleteTree(TreeNode* root) {
    if (root != nullptr) {
        deleteTree(root->left);
        deleteTree(root->right);
        delete root;
    }
}

int main() {
    TreeNode* root = nullptr;

    // 插入节点
    root = insertNode(root, 8);
    root = insertNode(root, 3);
    root = insertNode(root, 10);
    root = insertNode(root, 1);
    root = insertNode(root, 6);
    root = insertNode(root, 4);
    root = insertNode(root, 7);
    root = insertNode(root, 14);
    root = insertNode(root, 13);

    std::cout << "Preorder Traversal: ";
    preorderTraversal(root);
    std::cout << std::endl;

    std::cout << "Inorder Traversal: ";
    inorderTraversal(root);
    std::cout << std::endl;

    std::cout << "Postorder Traversal: ";
    postorderTraversal(root);
    std::cout << std::endl;

    // 删除节点
    root = deleteNode(root, 10);
    std::cout << "Inorder Traversal after deleting 10: ";
    inorderTraversal(root);
    std::cout << std::endl;

    // 释放内存
    deleteTree(root);

    return 0;
}
```

# 堆 Heap

1. **定义**：堆是一种特殊的完全二叉树，满足以下性质之一：
   - 大顶堆（Max Heap）：父节点的值总是大于或等于子节点的值
   - 小顶堆（Min Heap）：父节点的值总是小于或等于子节点的值
2. **方式方法**：堆通常用数组来实现，因为数组可以提供快速的随机访问。堆的数组实现中，对于任意给定的节点 i（数组索引从0开始），其左子节点的索引是 2 * i + 1，右子节点的索引是 2 * i + 2，父节点的索引是 (i-1)/2（向下取整）。
3. **应用**：堆的两个主要操作是插入和删除，这两个操作需要通过“上浮”（insertion）和“下沉”（deletion）来维护堆的性质。
   - **上浮**：当一个新元素被插入到堆中，它会被放在数组的末尾，然后与父节点比较，如果违反了堆的性质，就将它与父节点交换，直到满足堆的性质或到达根节点。
   - **下沉**：当堆顶元素被移除后，末尾的元素会被移动到根节点，然后与子节点比较，如果违反了堆的性质，就将它与子节点中较大的（大顶堆）或较小的（小顶堆）交换，直到满足堆的性质或到达叶子节点。

## 示例

```cpp
#include <iostream>
#include <queue>
#include <vector>
#include <functional> // For std::greater

int main() {
    // 创建一个小顶堆，通过传递std::greater<int>作为比较函数
    std::priority_queue<int, std::vector<int>, std::greater<int>> minHeap;

    // 向堆中插入元素
    minHeap.push(30);
    minHeap.push(10);
    minHeap.push(20);
    minHeap.push(40);

    // 打印堆顶元素（最小值）
    std::cout << "The minimum element is: " << minHeap.top() << std::endl;

    // 删除堆顶元素
    minHeap.pop();

    // 再次打印堆顶元素（新的最小值）
    std::cout << "The new minimum element is: " << minHeap.top() << std::endl;

    // 检查堆是否为空
    if (minHeap.empty()) {
        std::cout << "The heap is empty." << std::endl;
    } else {
        std::cout << "The heap is not empty." << std::endl;
    }

    return 0;
}

```

# 图 Graph

# 哈希表 Hash Table

1. **定义**：哈希表（Hash Table），也称为散列表，是一种通过哈希函数将键（Key）映射到表中一个位置以便快速访问记录的数据结构。它提供了快速的数据插入、删除和查找操作。哈希表的实现通常依赖于数组和链表（或其他数据结构）的组合。

2. **基本组成**：
- 哈希函数（Hash Function）：一个函数，它接受一个键作为输入，并返回一个整数值，这个值称为哈希值或哈希码。理想情况下，这个函数应该将键均匀地分布在哈希表中，以减少冲突。
- 数组（Array）：哈希表通常使用数组来存储数据，数组的索引由哈希函数的输出确定。
- 链表（Chaining）：当两个或多个键具有相同的哈希值时，会发生冲突。在这种情况下，可以使用链表来存储具有相同哈希值的所有元素。

## 示例

```cpp
#include <iostream>
#include <unordered_map>
#include <string>

int main() {
    // 创建一个哈希表，键为string类型，值为int类型
    std::unordered_map<std::string, int> hashTable;

    // 插入键值对
    hashTable["apple"] = 1;
    hashTable["banana"] = 2;
    hashTable["cherry"] = 3;

    // 访问和打印键"banana"对应的值
    std::cout << "Hash value for 'banana': " << hashTable["banana"] << std::endl;

    // 检查键是否存在
    if (hashTable.find("apple") != hashTable.end()) {
        std::cout << "Key 'apple' exists in hash table." << std::endl;
    }

    // 删除键"cherry"
    hashTable.erase("cherry");

    // 检查键"cherry"是否被删除
    if (hashTable.find("cherry") == hashTable.end()) {
        std::cout << "Key 'cherry' has been erased from hash table." << std::endl;
    }

    // 遍历哈希表
    std::cout << "Hash table contents:" << std::endl;
    for (const auto& pair : hashTable) {
        std::cout << pair.first << ": " << pair.second << std::endl;
    }

    // 获取哈希表的大小
    std::cout << "Hash table size: " << hashTable.size() << std::endl;

    return 0;
}
```

# 字典树 Trie

# 并查集 Union-Find
